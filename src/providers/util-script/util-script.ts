/**
    Author (s): Ankit Maheshwari, 
                Pradeep Choudhary
    Date:27DEC2016
    Copyright Notice: 
    @(#)
    Description:
*/

import { Injectable } from '@angular/core';
import { NavController} from 'ionic-angular';

@Injectable()
export class UtilScript {

	constructor(public nav: NavController) {
		this.nav = nav;
	}
	
	public goBack() {
		// navigates to the previous page in the stack,
		// which based on the model above is tab1Inner
		this.nav.pop();
	}

}