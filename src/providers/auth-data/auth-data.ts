/**
	Author (s): Ankit Maheshwari
	Date:27DEC2016
	Copyright Notice: 
	@(#)
	Description:
*/ 

import {NavController, AlertController, ToastController, LoadingController} from 'ionic-angular';
import {Injectable} from '@angular/core';
import {SignInPage} from '../../pages/sign-in/sign-in';

//import { Http } from '@angular/http';
//import 'rxjs/add/operator/map';

//import * as firebase from 'firebase';
declare var firebase: any;

/*
	Generated class for the AuthData provider.

	See https://angular.io/docs/ts/latest/guide/dependency-injection.html
	for more info on providers and Angular 2 DI.
*/

@Injectable()
export class AuthData {

	public FIRE_AUTH    : any;
	public USER_PROFILE : any;
	public CONTACT_USER : any;
	public CONTACT_USER_HOTEL : any;
	public HOTEL_INFO   : any;
	public ROOM_STATUS  : any;
	public CALL_LOGS    : any;
	public STORAGE_REF  : any;
	local: Storage;
	
	constructor(public nav: NavController,         public loadingCtrl: LoadingController, 
				public alertCtrl: AlertController, public toastCtrl: ToastController) {
		this.nav = nav;
		
		//creates a firebase auth reference, now you can get access to all the auth methods with this.FIRE_AUTH
		this.FIRE_AUTH = firebase.auth();
		//is creating a database reference to the USER_PROFILE node on my firebase database.
		this.USER_PROFILE = firebase.database().ref('/userProfile');
		this.CONTACT_USER = firebase.database().ref('/contactUser');
		this.CONTACT_USER_HOTEL = firebase.database().ref('/inHotelUser');
		this.HOTEL_INFO   = firebase.database().ref('/hotelInfo');
		this.ROOM_STATUS  = firebase.database().ref('/roomStatus');
		this.CALL_LOGS    = firebase.database().ref('/callLogs');
		this.STORAGE_REF  = firebase.storage().ref();
	}

		/**********************UTILITY METHODS**********************/
		getDate() {
			 return new Date().toString();
		}

		//#1
		//a logout function: that one doesn�t take any arguments 
		//it just checks for the current user and logs him out
		currentUser(): any {
			return firebase.auth().currentUser;
		}

		//#2
		//loginUser, it takes an email and a password, both strings.
		signinUser(email: string, password: string): any {
			//passing the email & password.
			var self = this;
			
			//Display loader
			var loading = this.loadingCtrl.create({
				content: "Loading..."
			});
			loading.present();
			
			return this.FIRE_AUTH.signInWithEmailAndPassword(email, password).then((authData) => {
				let navbar = this.nav;
				//console.log("authData.uid: "  + authData.uid);
				firebase.database().ref('/userProfile/' + authData.uid).once('value').then(function(snapshot) {
					let userInfo = snapshot.val();
					
					loading.dismiss();
					//Displaying toast to welcome user for Login!
					let toast = self.toastCtrl.create({
						message: 'Hi ' + userInfo.cName + '! Welcome back to CW :)',
						duration: 3000,
						position: 'bottom',
						showCloseButton: true,
						closeButtonText: 'OK'
					});
					toast.present();
				});
				
			}, (error) => {
				loading.dismiss();
				//If there�s a problem it�s going to show an alert to the user with the error message.
				let prompt = this.alertCtrl.create({
					message: error.message,
					buttons: [{text: "Ok"}]
				});
				prompt.present();
			});
		}

		//#3
		//let�s create a signup function now: 
		//function that takes email & password both strings.
		signupUser(email:      string, 
					password:  string,
					cName:     string, 
				contactnumber: string): any {
			//passing those to this.FIRE_AUTH.createUserWithEmailAndPassword()  
			//which handles the user creating logic for us.
			
			//Display loader
			var loading = this.loadingCtrl.create({
				content: "Loading..."
			});
			loading.present();
			
			var self = this;
			this.FIRE_AUTH.createUserWithEmailAndPassword(email, password).then((newUser) => {
				//we are calling this method to sent a link to user over mail - to verify their email address.
				newUser.sendEmailVerification(); 
				//we are calling the signInWithEmailAndPassword() on successful user creation. 
				this.FIRE_AUTH.signInWithEmailAndPassword(email, password).then((authenticatedUser) => {
					addUserNow("");
					//Function to add user's information.
					function addUserNow(profileImg) {
						//After the user is created and authenticated we need to create a USER_PROFILE  node for that user:
						self.USER_PROFILE.child(authenticatedUser.uid).set({
							uid:              authenticatedUser.uid,
							email:            email,
							cName:            cName,
							contactnumber:    contactnumber,
							profileImg:       profileImg, 
							isAdmin:          false, //By default user will not be Admin
							isVerified:       false, //By default user will not be Verified, Admin will verify user.
							createDate:       self.getDate(),
							updatedDate:      self.getDate()
						}).then(() => {
							
							//SigningOut user once created.
							self.FIRE_AUTH.signOut();
							
							let navbar = self.nav;
							//Displaying toast to welcome user for Signup!
							firebase.database().ref('/userProfile/' + authenticatedUser.uid).once('value').then(function(snapshot) {
								let userInfo = snapshot.val();
								
								//Hide loader
								setTimeout(()=>{
									loading.dismiss();
								},500);
								
								//Displaying toast to welcome user!
								let toast = self.toastCtrl.create({
									message: 'Hi ' + userInfo.cName + '! Welcome to CW :)',
									duration: 3000,
									position: 'bottom',
									showCloseButton: true,
									closeButtonText: 'OK'
								});
								toast.present();
								
								//an alert letting them know there was a problem.
								let prompt = self.alertCtrl.create({
									title: "Thanks for sign up!",
									message: "A verification link sent over email. Please verify your email address.",
									buttons: [{text: "Ok"}]
								});
								prompt.present();
							});
						});
					}
				})
			}, (error) => {
				//SigningOut user once created.
				self.FIRE_AUTH.signOut();
				//Removing loading..
				loading.dismiss();
				var errorMessage: string = error.message;
				let prompt = this.alertCtrl.create({
				    message: errorMessage,
				    buttons: [{text: "Ok"}]
				});
				prompt.present();
				//Hide loader
				setTimeout(()=>{
				    loading.dismiss();
				},1000);
			});
		}

		//#4
		//reset password function that takes an email as a string.
		resetPassword(email: string): any {
			/* We are passing that email to: 
				this.FIRE_AUTH.sendPasswordResetEmail(email) and 
				Firebase will take care of the reset login, 
				they send an email to your user with a password reset link, 
				the user follows it and changes his password without you breaking a sweat.
			*/
			var self = this;
			return this.FIRE_AUTH.sendPasswordResetEmail(email).then((user) => {
				//an alert letting your user know that email was sent.
				let prompt = this.alertCtrl.create({
					message: "We just sent you a reset link to your email",
					buttons: [{text: "Ok"}]
				});
				prompt.present();
			}, (error) => {
				/* created own errorMessage to show how we can personalize those messages to our users, 
					default error messages are great for developers, they let us know what�s going on, 
					but we should never show them to our users.
				*/
				var errorMessage: string;
				switch (error.code) {
					case "auth/invalid-email":
						errorMessage = "You'll need to write a valid email address";
						break;
					case "auth/user-not-found":
						errorMessage = "That user does not exist";
						break;
					default:
						errorMessage = error.message;
				}
				
				//an alert letting them know there was a problem.
				let prompt = this.alertCtrl.create({
					message: errorMessage,
					buttons: [{text: "Ok"}]
				});
				prompt.present();
			});
		}

		//#5
		//a logout function: that one doesn�t take any arguments 
		//it just checks for the current user and logs him out
		logoutUser(): any {
			var self = this;
			return this.FIRE_AUTH.signOut().then(function() {
				// Sign-out successful.
				self.nav.setRoot(SignInPage);
			}, function(error) {
				// An error happened.
			});
		}
		
		//#6.1
		//let�s create a addContact function now: 
		//function that takes name & contactnumber etc as strings.
		addContactUser(
                    callback: any,
                    name:        string, 
					contactnumber:  string, 
					address:        string, 
					city:           string, 
					arrivedBy:      string, 
					arrivalDetail:  string,
					arrivalTime:    string,
					checkInOut:     string,
					bookingForDay:  any,
					filled:         number, 
					remark:         string
						): any {
			//Function to add user's information.
			var self = this;
			
			//Display loader
			var loading = this.loadingCtrl.create({
				content: "Loading..."
			});
			loading.present();
			
			var currentUserObj = this.currentUser();
			//console.log(currentUserObj.uid);
			
			//After the user is created and authenticated we need to create a CONTACT_USER  node for that user:
			self.CONTACT_USER.push({
				totalCalls:       0,
				name:             name,
				contactnumber:    contactnumber,
				address:          address, 
				city:             city.toLowerCase(), 
				arrivedBy:        arrivedBy, 
				arrivalDetail:    arrivalDetail, 
				arrivalTime:      arrivalTime,
				checkInOut:       checkInOut,
				bookingForDay:    bookingForDay,
				filled:           filled,
				remark:           remark,
				isdeleted:        false, //By default user will not be Deleted
				isVerified:       false, //By default user will not be Verified, Admin will verify user.
				createDate:       self.getDate(),
				updatedDate:      self.getDate(),
				currentUser:      currentUserObj.uid
			}).then(() => {
				//Hide loader
				setTimeout(()=>{
					loading.dismiss();
				},500);
				
				//Displaying toast to welcome user!
				let toast = self.toastCtrl.create({
					message: 'New contact has been added!',
					duration: 3000,
					position: 'bottom',
					showCloseButton: true,
					closeButtonText: 'OK'
				});
				toast.present();
                callback(true);
			});
		}

		//#6.2
		//let�s create a addContact function now: 
		//function that takes name & contactnumber etc as strings.
		addContactUserInHotel(
                    callback: any,
                    name:        string, 
					contactnumber:  string, 
					address:        string, 
					city:           string, 
					arrivedBy:      string, 
					arrivalDetail:  string,
					arrivalTime:    string,
					checkInOut:     string,
					bookingForDay:  any,
					filled:         number, 
					remark:         string
						): any {
			//Function to add user's information.
			var self = this;
			
			//Display loader
			var loading = this.loadingCtrl.create({
				content: "Loading..."
			});
			loading.present();
			
			var currentUserObj = this.currentUser();
			//console.log(currentUserObj.uid);
			
			//After the user is created and authenticated we need to create a CONTACT_USER  node for that user:
			self.CONTACT_USER_HOTEL.push({
				totalCalls:       0,
				name:             name,
				contactnumber:    contactnumber,
				address:          address, 
				city:             city.toLowerCase(), 
				arrivedBy:        arrivedBy, 
				arrivalDetail:    arrivalDetail, 
				arrivalTime:      arrivalTime,
				checkInOut:       checkInOut,
				bookingForDay:    bookingForDay,
				filled:           filled,
				remark:           remark,
				isdeleted:        false, //By default user will not be Deleted
				isVerified:       false, //By default user will not be Verified, Admin will verify user.
				createDate:       self.getDate(),
				updatedDate:      self.getDate(),
				currentUser:      currentUserObj.uid
			}).then(() => {
				//Hide loader
				setTimeout(()=>{
					loading.dismiss();
				},500);
				
				//Displaying toast to welcome user!
				let toast = self.toastCtrl.create({
					message: 'New contact has been added!',
					duration: 3000,
					position: 'bottom',
					showCloseButton: true,
					closeButtonText: 'OK'
				});
				toast.present();
                callback(true);
			});
		}

		//#7
		//let�s create a addHotelInfo function now: 
		//function that takes name & contactnumber etc as strings.
		addHotelInfo(hotelName:    string, 
						address:   string, 
						city:      string
						): any {
			//Function to add user's information.
			var self = this;
			
			//Display loader
			var loading = this.loadingCtrl.create({
			    content: "Loading..."
			});
			loading.present();
			
			var currentUserObj = this.currentUser();
			//console.log(currentUserObj);
			
			//After the user is created and authenticated we need to create a CONTACT_USER  node for that user:
			self.HOTEL_INFO.set({
				hotelName:        hotelName,
				address:          address, 
				city:             city.toLowerCase(), 
				isAdmin:          false, //By default user will not be Admin
				isVerified:       false, //By default user will not be Verified, Admin will verify user.
				createDate:       self.getDate(),
				updatedDate:      self.getDate(),
				currentUser:      currentUserObj.uid
			}).then(() => {
				//And after that happens it sends the user to the apps HomePage
				/* If you are wondering why I�m using this.nav.setRoot(HomePage)  
					instead of this.nav.push(HomePage) it�s easy, I want HomePage to be my rootPage, 
					that means it�s going to be the only page on the stack, no back button or anything like that.
				*/
				//Hide loader
				setTimeout(()=>{
					loading.dismiss();
				},500);
				
				//Displaying toast to welcome user!
				let toast = self.toastCtrl.create({
					message: 'Hotel Information has been added!',
					duration: 3000,
					position: 'bottom',
					showCloseButton: true,
					closeButtonText: 'OK'
				});
				toast.present();
			});
		}

		//#8.1
		//let�s create a editContact function now: 
		//function that takes name & contactnumber etc as strings.
		editContact(callback:    any,
					contactId:   string,
					selectHotel: number, 
					arrivedBy:   string, 
					arrivalDetail:   string,
					arrivalTime: string,
					roomNumber:  string,  
					checkInOut:  string,
					bookingForDay:   any,
					filled:          number,
					remark:      string  
		): any {
			//Function to add user's information.
			var self = this;
			
			//Display loader
			var loading = this.loadingCtrl.create({
				content: "Loading..."
			});
			loading.present();
			
			var currentUserObj = this.currentUser();
			
			if(contactId != null) {
				//After the user is created and authenticated we need to create a CONTACT_USER  node for that user:
				self.CONTACT_USER.child(contactId).update({
					selectHotel:     Number(selectHotel),
					arrivedBy:       arrivedBy,
					arrivalDetail:   arrivalDetail,
					arrivalTime:     arrivalTime,
					roomNumber:      roomNumber,
					checkInOut:      checkInOut,
					bookingForDay:   bookingForDay,
					filled:          filled,
					remark:          remark, 
					updatedDate:     self.getDate(),
					currentUser:     currentUserObj.uid
				}).then(() => {
					//And after that happens it sends the user to the apps HomePage
					/* If you are wondering why I�m using this.nav.setRoot(HomePage)  
						instead of this.nav.push(HomePage) it�s easy, I want HomePage to be my rootPage, 
						that means it�s going to be the only page on the stack, no back button or anything like that.
					*/
					
					//Displaying toast to welcome user!
					let toast = self.toastCtrl.create({
						message: 'User info updated',
						duration: 3000,
						position: 'bottom',
						showCloseButton: true,
						closeButtonText: 'OK'
					});
					toast.present();
					
					//Hide loader
					setTimeout(()=>{
						loading.dismiss();
						callback(true);
					},500);

				});
			}
		}

		//#8.2
		//let�s create a editContact function now: 
		//function that takes name & contactnumber etc as strings.
		editContactInHotel(callback:    any,
					contactId:   string,
					selectHotel: number, 
					arrivedBy:   string, 
					arrivalDetail:   string,
					arrivalTime: string,
					roomNumber:  string,  
					checkInOut:  string,
					bookingForDay:   any,
					filled:          number,
					remark:      string  
		): any {
			//Function to add user's information.
			var self = this;
			
			//Display loader
			var loading = this.loadingCtrl.create({
				content: "Loading..."
			});
			loading.present();
			
			var currentUserObj = this.currentUser();
			
			if(contactId != null) {
				//After the user is created and authenticated we need to create a CONTACT_USER  node for that user:
				self.CONTACT_USER_HOTEL.child(contactId).update({
					selectHotel:     Number(selectHotel),
					arrivedBy:       arrivedBy,
					arrivalDetail:   arrivalDetail,
					arrivalTime:     arrivalTime,
					roomNumber:      roomNumber,
					checkInOut:      checkInOut,
					bookingForDay:   bookingForDay,
					filled:          filled,
					remark:          remark, 
					updatedDate:     self.getDate(),
					currentUser:     currentUserObj.uid
				}).then(() => {
					//And after that happens it sends the user to the apps HomePage
					/* If you are wondering why I�m using this.nav.setRoot(HomePage)  
						instead of this.nav.push(HomePage) it�s easy, I want HomePage to be my rootPage, 
						that means it�s going to be the only page on the stack, no back button or anything like that.
					*/
					
					//Displaying toast to welcome user!
					let toast = self.toastCtrl.create({
						message: 'User info updated',
						duration: 3000,
						position: 'bottom',
						showCloseButton: true,
						closeButtonText: 'OK'
					});
					toast.present();
					
					//Hide loader
					setTimeout(()=>{
						loading.dismiss();
						callback(true);
					},500);

				});
			}
		}
		
		//#9
		//let�s create a UpdateRoomStatus function now: 
		//function that takes hotelId & roomNumber etc as strings.
		updateRoomStatus(
					callback:   any,
					hotelId:    string,
					roomNumber: string, 
					filled:     number
		): any {
			//Function to add user's information.
			var self = this;
			
			var currentUserObj = this.currentUser();
			//console.log(currentUserObj);
			self.getRoomStatus(
				function(snapshot) {
					let data = snapshot.val();
					let filledPrev = 0;
					if(data != null) {
						if(data[hotelId]) {
							if(data[hotelId][roomNumber]) {
								filledPrev = data[hotelId][roomNumber]['filled'];
							}
						}
					}
					
					filled = Number(filledPrev) + Number(filled);
					
					//After the user is created and authenticated we need to create a CONTACT_USER  node for that user:
					self.ROOM_STATUS.child(hotelId).child(roomNumber).set({
						hotelId:          hotelId,
						roomNumber:       roomNumber, 
						filled:           filled,
						createDate:       self.getDate(),
						updatedDate:      self.getDate(),
						currentUser:      currentUserObj.uid
					}).then(() => {
						//And after that happens it sends the user to the apps HomePage
						/* If you are wondering why I�m using this.nav.setRoot(HomePage)  
							instead of this.nav.push(HomePage) it�s easy, I want HomePage to be my rootPage, 
							that means it�s going to be the only page on the stack, no back button or anything like that.
						*/
						//Displaying toast to welcome user!
						let toast = self.toastCtrl.create({
							message: 'Room status updated',
							duration: 3000,
							position: 'bottom',
							showCloseButton: true,
							closeButtonText: 'OK'
						});
						toast.present();
						callback(true);
					});
			}, hotelId, roomNumber);
		}
		
		//#10.1
		/*Method: Contacts
		  Fetch list of all Contacts*/
		getContacts(callback: Function, limitTo: number, city: string): any {
			//Display loader
			var loading = this.loadingCtrl.create({
				content: "Loading..."
			});
			loading.present();
			
            if(city != '') {
    			this.CONTACT_USER.orderByChild("city").equalTo(city.toLowerCase()).on('value', function(snapshot) {
    				//Hide loader
    				loading.dismiss();
    				callback(snapshot);
    			});
            } else {
                this.CONTACT_USER.on('value', function(snapshot) {
                    //Hide loader
                    loading.dismiss();
                    callback(snapshot);
                });
            }
		}

		//#10.2
		/*Method: Contacts
		  Fetch list of all Contacts*/
		getContactsInHotel(callback: Function, limitTo: number): any {
			//Display loader
			var loading = this.loadingCtrl.create({
				content: "Loading..."
			});
			loading.present();
			
			this.CONTACT_USER_HOTEL.on('value', function(snapshot) {
                //Hide loader
                loading.dismiss();
                callback(snapshot);
            });
		}

		//#11.1
		/*Method: Contacts
			Fetch list of all Contacts*/
		getContact(callback: Function, contactId: string): any {
			this.CONTACT_USER.child("/" + contactId).once('value').then(function(snapshot) {
				callback(snapshot);
			});
		}
		
		//#11.2
		/*Method: Contacts
			Fetch list of all Contacts*/
		getContactInHotel(callback: Function, contactId: string): any {
			this.CONTACT_USER_HOTEL.child("/" + contactId).once('value').then(function(snapshot) {
				callback(snapshot);
			});
		}
		
		//#12
		/*Method: Contacts
			Fetch list of all Contacts*/
		getRoomStatus(
					callback: Function,
					hotelId:    any,
					roomNumber: any
		): any {
			this.ROOM_STATUS.child("/" + hotelId).child("/" + roomNumber).once('value').then(function(snapshot) {
				callback(snapshot);
			});
		}
		
		
		//#13
		/*Method: Contacts
			Fetch list of all RoomStatusList*/
		getRoomStatusList(callback: Function): any {
			this.ROOM_STATUS.on('value', function(snapshot) {
				callback(snapshot);
			});
		}
		
		//#14
		//let�s create a editContact function now: 
		//function that takes name & contactnumber etc as strings.
		callMade(callback: any, contactId: any, 
			contactnumber: string, totalCalls: number): any {
			//Function to add user's information.
			var self = this;
			
			var currentUserObj = this.currentUser();
			
			if(contactId != null) {
                self.getContact(function(snapshot) {
                    var data = snapshot.val();
    				//After the user is created and authenticated we need to create a CONTACT_USER  node for that user:
    				self.CONTACT_USER.child(contactId).update({
    					totalCalls: (data.totalCalls + totalCalls)
    				}).then(() => {
    					//Adding Call logs..
    					self.CALL_LOGS.child(contactId).push({
    		                contactnumber: contactnumber,
    		                createDate:    self.getDate(),
    		                updatedDate:   self.getDate(),
    						currentUser:   currentUserObj.uid
    		            }).then(() => {
    		                callback(true);
    		            });
    					
    				});
                }, contactId);
			}
		}
		
		//#15
		/*Method: getContactsInRoom
		  Fetch list of all Contacts stayed in Room*/
		getContactsInRoom(callback: Function, hotelId: number): any {
			//Display loader
			var loading = this.loadingCtrl.create({
				content: "Loading..."
			});
			loading.present();
			this.CONTACT_USER_HOTEL.orderByChild("selectHotel").equalTo(hotelId).on('value', function(snapshot) {
                //Hide loader
                loading.dismiss();
                callback(snapshot);
            });
		}
}