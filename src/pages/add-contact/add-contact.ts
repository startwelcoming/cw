/**
    Author (s): Ankit Maheshwari, 
                Pradeep Choudhary
    Date:27DEC2016
    Copyright Notice: 
    @(#)
    Description:
*/

import { Component } from '@angular/core';
import { ModalController, NavController, Toast, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthData } from '../../providers/auth-data/auth-data';
import { ContactsPage } from '../contacts/contacts';
import * as CMN from '../cmn/cmn';

@Component({
  templateUrl: 'add-contact.html',
	providers: [AuthData]
})

export class AddContactPage {
	
	public event = {
		timeStarts: '07:43',
	}
	CMN: any;
	addContactForm: FormGroup;
	
	constructor(public modalCtrl: ModalController, public nav: NavController,
		public authData: AuthData, public formBuilder: FormBuilder,
		public alertCtrl: AlertController) {

		this.CMN = CMN;
		this.nav = nav;
		this.authData = authData;

		this.addContactForm = formBuilder.group({
			name:               ['', Validators.required],
			contactnumber:      ['', Validators.required],
			address:            ['', Validators.required],
			city:               ['', Validators.required],
			arrivedBy:          ['', Validators.required],
			arrivalDetail:        ['', Validators.required],
			arrivalTime:        ['', Validators.required],
			checkInOut:              ['', Validators.required],
			bookingForDay1:          ['', Validators.required],
			bookingForDay2:          ['', Validators.required],
			filled:                  ['', Validators.required],
			remark:                  ['', Validators.required],
		})
	}
	
	//the addContact function:
	addContactUser(event) {
        var self = this;
		event.preventDefault();
		
		this.addContactForm.value.arrivedBy 
			= this.addContactForm.value.arrivedBy ? this.addContactForm.value.arrivedBy : '';
		
		this.addContactForm.value.arrivalTime 
			= this.addContactForm.value.arrivalTime ? this.addContactForm.value.arrivalTime : '';
		
		this.authData.addContactUserInHotel(
            function() {
                self.nav.setRoot(ContactsPage);
            },
            this.addContactForm.value.name,      
			this.addContactForm.value.contactnumber,
			this.addContactForm.value.address ? this.addContactForm.value.address : '',
			this.addContactForm.value.city,
			this.addContactForm.value.arrivedBy,
			this.addContactForm.value.arrivalDetail, 
			this.addContactForm.value.arrivalTime,
			this.addContactForm.value.checkInOut ? this.addContactForm.value.checkInOut : '',
			{
				1: this.addContactForm.value.bookingForDay1 ? this.addContactForm.value.bookingForDay1 : false,
				2: this.addContactForm.value.bookingForDay2 ? this.addContactForm.value.bookingForDay2 : false
			},
			this.addContactForm.value.filled ? this.addContactForm.value.filled : '',
			this.addContactForm.value.remark ? this.addContactForm.value.remark: ''
		);
	}

}