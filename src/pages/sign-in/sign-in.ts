/**
    Author (s): Ankit Maheshwari, 
                Pradeep Choudhary
    Date:27DEC2016
    Copyright Notice: 
    @(#)
    Description:
*/

import { Component } from '@angular/core';
import { ModalController, NavController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthData } from '../../providers/auth-data/auth-data';
import { ContactsPage } from '../contacts/contacts';
import * as CMN from '../cmn/cmn';

declare var firebase: any;

@Component({
    templateUrl: 'sign-in.html',
    providers: [AuthData]
})

export class SignInPage {
	
	CMN: any;
	showLogin: boolean;
    signinForm: FormGroup;
	
	constructor(public modalCtrl: ModalController, public nav: NavController,
				public authData: AuthData, public formBuilder: FormBuilder) {
		
		this.CMN = CMN;
		this.showLogin = true;;
        this.authData = authData;
		
        this.signinForm = formBuilder.group({
            email:            ['', Validators.required],
            password:         ['', Validators.required],
        })
	}
	
	
	//the signup function:
	signinUser(event) {
		event.preventDefault();
		
		this.authData.signinUser(this.signinForm.value.email, this.signinForm.value.password);
	}
	
	//to open more info toggle
	toggleShowLogin() {
		if(this.showLogin) {
			this.showLogin = false;
		} else {
			this.showLogin = true;
		}
	}
}