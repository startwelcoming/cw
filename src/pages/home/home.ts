/**
    Author (s): Ankit Maheshwari, 
                Pradeep Choudhary
    Date:27DEC2016
    Copyright Notice: 
    @(#)
    Description:
*/

import { Component } from '@angular/core';
import { ModalController, NavController, LoadingController } from 'ionic-angular';
import { AuthData } from '../../providers/auth-data/auth-data';
import { ContactsPage } from '../contacts/contacts';
import * as CMN from '../cmn/cmn';

declare var firebase: any;

@Component({
    templateUrl: 'home.html',
    providers: [AuthData]
})

export class HomePage {
	
	constructor(public nav: NavController,
				public authData: AuthData,
				public loadingCtrl: LoadingController) {
		
        this.authData = authData;
		
        //Display loader
        var loading = this.loadingCtrl.create({
            content: "Loading..."
        });
        loading.present();
		
        let self = this;
   		firebase.auth().onAuthStateChanged((user) => {
			//Set different Pages link - which depends on type of user loggedin.
            loading.dismiss();
			if(user) {
				self.nav.setRoot(ContactsPage);
			} else {
				//Sigin page called means user must logged-out
				self.authData.logoutUser();
			}
        });
	}
}