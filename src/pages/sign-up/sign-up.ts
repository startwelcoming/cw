/**
    Author (s): Ankit Maheshwari, 
                Pradeep Choudhary
    Date:27DEC2016
    Copyright Notice: 
    @(#)
    Description:
*/

import { Component } from '@angular/core';
import { ModalController, NavController, Toast } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthData } from '../../providers/auth-data/auth-data';
import * as CMN from '../cmn/cmn';


@Component({
  templateUrl: 'sign-up.html',
    providers: [AuthData]
})

export class SignUpPage {
	
	CMN: any;
    signupForm: FormGroup;
	
	constructor(public modalCtrl: ModalController, public nav: NavController, 
                public authData: AuthData, public formBuilder: FormBuilder) {
        
		this.CMN = CMN;
        this.nav = nav;
        this.authData = authData;
        
        this.signupForm = formBuilder.group({
            email:            ['', Validators.required],
            password:         ['', Validators.required],
            cName:            ['', Validators.required],
            contactnumber:    ['', Validators.required],
        })
	}
	
    //the signup function:
    signupUser(event) {
        event.preventDefault();
       
        this.authData.signupUser(this.signupForm.value.email, this.signupForm.value.password,
                                 this.signupForm.value.cName, this.signupForm.value.contactnumber);
    }

}