/**
    Author (s): Ankit Maheshwari, 
                Pradeep Choudhary
    Date:28DEC2016
    Copyright Notice: 
    @(#)
    Description:
*/

import { Component } from '@angular/core';
import { AuthData } from '../../providers/auth-data/auth-data';
import * as CMN from '../cmn/cmn';

declare var firebase: any;

@Component({
  templateUrl: 'log-out.html',
    providers: [AuthData]
})

export class LogOutPage {
	
	constructor(public authData: AuthData) {
		//Sigin page called means user must logged-out
		this.authData.logoutUser();
	}
}