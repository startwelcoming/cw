import { Component } from '@angular/core';
import { ModalController, Platform, NavParams, ViewController } from 'ionic-angular';

'use strict';

export var hotelName = {
	'1': 'Jalsa - JNW (New Wing)',
	'2': 'Jalsa - JOW (Old Wing)',
	'3': 'Jalsa - JD  (Dormitories)',
	'4': 'Jalsa - JS  (Suite)',
	'5': 'President',
	'6': 'EFFOTEL',
	'7': 'SILVOTEL'
}

export var hotels = [
	{
		hotelId: 1,
		hotelName:    hotelName['1']
	},
	{
		hotelId: 2,
		hotelName:    hotelName['2']
	},
	{
		hotelId: 3,
		hotelName:    hotelName['3']
	},
	{
		hotelId: 4,
		hotelName:    hotelName['4']
	},
	{
		hotelId: 5,
		hotelName:    hotelName['5']
	},
    {
        hotelId: 6,
        hotelName:    hotelName['6']
    },
    {
        hotelId: 7,
        hotelName:    hotelName['7']
    }
]

export var hotelRooms =
	{
		1: [
			{
				roomNo:   1001,
				capacity: 3
			},
			{
				roomNo:   1002,
				capacity: 3
			},
			{
				roomNo:   1003,
				capacity: 3
			},
			{
				roomNo:   1004,
				capacity: 3
			},
			{
				roomNo:   1005,
				capacity: 3
			},
			{
				roomNo:   1006,
				capacity: 3
			},
			{
				roomNo:   1007,
				capacity: 3
			},
			{
				roomNo:   1008,
				capacity: 3
			},
			{
				roomNo:   1009,
				capacity: 3
			},
			{
				roomNo:   1010,
				capacity: 3
			},
			{
				roomNo:   1011,
				capacity: 3
			},
			{
				roomNo:   1012,
				capacity: 3
			},
			{
				roomNo:   1013,
				capacity: 3
			},
			{
				roomNo:   1014,
				capacity: 3
			},
			{
				roomNo:   1015,
				capacity: 3
			},
			{
				roomNo:   1016,
				capacity: 3
			},
			{
				roomNo:   1017,
				capacity: 3
			},
			{
				roomNo:   2001,
				capacity: 3
			},
			{
				roomNo:   2002,
				capacity: 3
			},
			{
				roomNo:   2003,
				capacity: 3
			},
			{
				roomNo:   2004,
				capacity: 3
			},
			{
				roomNo:   2005,
				capacity: 3
			},
			{
				roomNo:   2006,
				capacity: 3
			},
			{
				roomNo:   2007,
				capacity: 3
			},
			{
				roomNo:   2008,
				capacity: 3
			},
			{
				roomNo:   2009,
				capacity: 3
			},
			{
				roomNo:   2010,
				capacity: 3
			},
			{
				roomNo:   2011,
				capacity: 3
			},
			{
				roomNo:   2012,
				capacity: 3
			},
			{
				roomNo:   2013,
				capacity: 3
			},
			{
				roomNo:   2014,
				capacity: 3
			},
			{
				roomNo:   2015,
				capacity: 3
			},
			{
				roomNo:   2016,
				capacity: 3
			},
			{
				roomNo:   2017,
				capacity: 3
			}
		],
		
		2:   [
			{
				roomNo:   101,
				capacity: 3
			},
			{
				roomNo:   102,
				capacity: 3
			},
			{
				roomNo:   103,
				capacity: 3
			},
			{
				roomNo:   104,
				capacity: 3
			},
			{
				roomNo:   105,
				capacity: 3
			},
			{
				roomNo:   106,
				capacity: 3
			},
			{
				roomNo:   107,
				capacity: 3
			},
			{
				roomNo:   108,
				capacity: 3
			},
			{
				roomNo:   201,
				capacity: 5
			},
			{
				roomNo:   202,
				capacity: 5
			},
			{
				roomNo:   203,
				capacity: 5
			},
			{
				roomNo:   204,
				capacity: 5
			},
			{
				roomNo:   205,
				capacity: 5
			},
			{
				roomNo:   206,
				capacity: 5
			},
			{
				roomNo:   207,
				capacity: 5
			},
			{
				roomNo:   208,
				capacity: 5
			},
			{
				roomNo:   209,
				capacity: 5
			},
			{
				roomNo:   210,
				capacity: 5
			},
			{
				roomNo:   211,
				capacity: 5
			},
			{
				roomNo:   212,
				capacity: 5
			},
			{
				roomNo:   213,
				capacity: 5
			},
			{
				roomNo:   214,
				capacity: 5
			},
			{
				roomNo:   215,
				capacity: 5
			},
			{
				roomNo:   216,
				capacity: 5
			},
			{
				roomNo:   217,
				capacity: 5
			}
		],
		
		3:   [
			{
				roomNo:   1,
				capacity: 14
			},
			{
				roomNo:   2,
				capacity: 14
			},
		],
		
		4:   [
			{
				roomNo:   1,
				capacity: 3
			},
			{
				roomNo:   2,
				capacity: 3
			},
		],
		
		5:   [
			{
				roomNo:   101,
				capacity: 5
			},
			{
				roomNo:   102,
				capacity: 5
			},
			{
				roomNo:   201,
				capacity: 5
			},
			{
				roomNo:   202,
				capacity: 5
			},
			{
				roomNo:   203,
				capacity: 5
			},
			{
				roomNo:   204,
				capacity: 5
			},
			{
				roomNo:   205,
				capacity: 5
			},
			{
				roomNo:   206,
				capacity: 5
			},
			{
				roomNo:   207,
				capacity: 5
			},
			{
				roomNo:   208,
				capacity: 5
			},
			{
				roomNo:   209,
				capacity: 5
			},
			{
				roomNo:   210,
				capacity: 5
			},
			{
				roomNo:   211,
				capacity: 5
			},
			{
				roomNo:   212,
				capacity: 5
			}
		],
        
        6:   [
            {
                roomNo:   1,
                capacity: 3
            },
            {
                roomNo:   2,
                capacity: 3
            },
            {
                roomNo:   3,
                capacity: 3
            },
            {
                roomNo:   4,
                capacity: 3
            },
            {
                roomNo:   5,
                capacity: 3
            },
            {
                roomNo:   6,
                capacity: 3
            },
            {
                roomNo:   7,
                capacity: 3
            },
            {
                roomNo:   8,
                capacity: 3
            },
            {
                roomNo:   9,
                capacity: 3
            },
            {
                roomNo:   10,
                capacity: 3
            },
            {
                roomNo:   11,
                capacity: 3
            },
            {
                roomNo:   12,
                capacity: 3
            },
            {
                roomNo:   13,
                capacity: 3
            },
            {
                roomNo:   14,
                capacity: 3
            }
        ],
        
        7:   [
            {
                roomNo:   1,
                capacity: 3
            },
            {
                roomNo:   2,
                capacity: 3
            },
            {
                roomNo:   3,
                capacity: 3
            },
            {
                roomNo:   4,
                capacity: 3
            },
            {
                roomNo:   5,
                capacity: 3
            },
            {
                roomNo:   6,
                capacity: 3
            },
            {
                roomNo:   7,
                capacity: 3
            },
            {
                roomNo:   8,
                capacity: 3
            },
            {
                roomNo:   9,
                capacity: 3
            },
            {
                roomNo:   10,
                capacity: 3
            },
            {
                roomNo:   11,
                capacity: 3
            },
            {
                roomNo:   12,
                capacity: 3
            },
            {
                roomNo:   13,
                capacity: 3
            },
            {
                roomNo:   14,
                capacity: 3
            },
            {
                roomNo:   15,
                capacity: 3
            },
            {
                roomNo:   16,
                capacity: 3
            },
            {
                roomNo:   17,
                capacity: 3
            }
        ]
	}


//Global variable, accessible throughout the app.
export var CWLOGO = "";

export class CMNClass {
	
	constructor() {}
}