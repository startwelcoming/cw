/**
    Author (s): Ankit Maheshwari, 
                Pradeep Choudhary
    Date:27DEC2016
    Copyright Notice: 
    @(#)
    Description:
*/

import { CallNumber } from 'ionic-native';
import { Component } from '@angular/core';
import { ModalController,NavParams} from 'ionic-angular';
import { AuthData } from '../../providers/auth-data/auth-data';
import * as CMN from '../cmn/cmn';

declare var firebase: any;

@Component({
    templateUrl: 'room.html',
    providers: [AuthData]
})

export class RoomPage {
	CMN: any;
	roomContactsList:any = [];
	roomContactsListBackup:any = [];
	hotels        :any = [];
	hotelsBackup  :any = [];
	hotelRooms    :any = [];
	hotelId:any;
	roomNo: any;
	
	constructor(public modalCtrl: ModalController,
				public authData: AuthData,
				public navParams: NavParams ) {
		
		this.CMN = CMN;
		
		this.roomNo  = navParams.get('roomNo');
		this.hotelId = navParams.get('hotelId');
		
		var self = this;
		//Fetching Current Room Status
		this.authData.getContactsInRoom(function(snapshot) {
			let data = snapshot.val();
            for(let key in data) {
                let value = data[key];
				if(value.roomNumber == self.roomNo) {
	                value.contactId = key;
	                self.roomContactsList.push(value);
				}
				self.roomContactsListBackup = self.roomContactsList; 
            }
		}, this.hotelId);
	}
    
	callNow(contact: any) {
		var self = this;
		CallNumber.callNumber(contact.contactnumber, true)
			.then(() => {
				//console.log('Launched dialer!');
				self.callMade(contact);
			})
			.catch(() => {
				//console.log('Error launching dialer')
			});
	}
	
	//the addContact function:
	callMade(contactTemp: any) {
		var self = this;
		
		var contact = {
			contactId:     '',
			contactnumber: '',
			totalCalls:    0
		}
		
		contact = contactTemp;
		
		contact.totalCalls = contact.totalCalls + 1;
		this.authData.callMade(
			function(isSuccess) {
				//Displaying toast for Call Started!
			    let toast = this.toastCtrl.create({
			        message: 'Call Started..!',
			        duration: 3000,
			        position: 'bottom',
			        showCloseButton: true,
			        closeButtonText: 'OK'
			    });
			    toast.present();
			},
			contact.contactId,
			contact.contactnumber,
			contact.totalCalls
		);
	}
	
    getReport(ev: any) {
        this.roomContactsList  = this.roomContactsListBackup;
        
        // set val to the value of the searchbar
        let val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.roomContactsList = this.roomContactsList.filter((item) => {
                var filteredData = (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
                if(!filteredData && item.city) {
                    filteredData = (item.city.toLowerCase().indexOf(val.toLowerCase()) > -1);
                }
                if(!filteredData && item.arrivedBy) {
                    filteredData = (item.arrivedBy.toLowerCase().indexOf(val.toLowerCase()) > -1);
                }
                if(!filteredData && item.contactnumber) {
                    filteredData = (item.contactnumber.toString().indexOf(val.toString()) > -1);
                }
                if(!filteredData && item.selectHotel) {
                    filteredData = (CMN.hotelName[item.selectHotel].toLowerCase().indexOf(val.toLowerCase()) > -1);
                }
            })
        }
    }
}