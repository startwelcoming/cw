/**
    Author (s): Ankit Maheshwari, 
                Pradeep Choudhary
    Date:27DEC2016
    Copyright Notice: 
    @(#)
    Description:
*/

import { Component } from '@angular/core';
import { NavController, ModalController,} from 'ionic-angular';
import { RoomPage } from '../room/room';
import { AuthData } from '../../providers/auth-data/auth-data';
import * as CMN from '../cmn/cmn';

declare var firebase: any;

@Component({
    templateUrl: 'report.html',
    providers: [AuthData]
})

export class ReportPage {
	CMN: any;
	roomStatusList:any = [];
	hotels        :any = [];
	hotelsBackup  :any = [];
	hotelRooms    :any = [];
	
	constructor( public nav: NavController, public modalCtrl: ModalController, public authData: AuthData ) {
		
		this.CMN = CMN;
		this.nav = nav;
		
		var self = this;
		//Fetching Current Room Status
		this.authData.getRoomStatusList(function(snapshot) {
			let data = snapshot.val();
			if(data) {
				self.roomStatusList = data;
			}
			self.hotels         = self.CMN.hotels;
			self.hotelsBackup   = self.CMN.hotels;
			self.hotelRooms     = self.CMN.hotelRooms;
		});
	}
    
    getReport(ev: any) {
        this.hotels  = this.hotelsBackup;
        
        // set val to the value of the searchbar
        let val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.hotels = this.hotels.filter((item) => {
                var filteredData = (item.hotelName.toLowerCase().indexOf(val.toLowerCase()) > -1);
                return filteredData;
            })
        }
    }

	//we are sending the user to the openRoomPage: by User
	openRoom(hotelId: number, roomNo: string){
		this.nav.push(RoomPage, {hotelId: hotelId, roomNo:roomNo});
	}	

}