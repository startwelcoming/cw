/**
    Author (s): Ankit Maheshwari, 
                Pradeep Choudhary
    Date:27DEC2016
    Copyright Notice: 
    @(#)
    Description:
*/

import { CallNumber } from 'ionic-native';
import { Component } from '@angular/core';
import { ModalController, NavController, ToastController, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthData } from '../../providers/auth-data/auth-data';
import { EditContactPage } from '../edit-contact/edit-contact';
import { AddContactPage }  from '../add-contact/add-contact';
import { ReportPage } from '../report/report';
import * as CMN from '../cmn/cmn';

declare var firebase: any;

@Component({
  template: `
	<ion-tabs>
		<ion-tab [root]="inHotelPage"
			tabTitle="IN HOTEL">
		</ion-tab>
		<ion-tab [root]="outCityPage"
			tabTitle="OUT CITY">
		</ion-tab>
		<ion-tab [root]="inCityPage"
			tabTitle="IN CITY">
		</ion-tab>
	</ion-tabs>`
})

export class ContactsPage {
	
	CMN: any;
	private inHotelPage: any;
	private inCityPage: any;
	private outCityPage: any;
	
	constructor(public modalCtrl: ModalController, 
        public nav: NavController,
        public formBuilder: FormBuilder, 
        public toastCtrl: ToastController,
        public loadingCtrl: LoadingController) {
		
		this.inHotelPage = InHotelPage;
		this.inCityPage  = InCityPage;
		this.outCityPage = OutCityPage;

		this.CMN = CMN;
		this.nav = nav;
	}
	
}

/*

*/
@Component({
  templateUrl:  'in-hotel.html',
	providers: [AuthData]
})
export class InHotelPage {

	CMN:      any;
	private sliderImages: Array<string>;
	imageOrVideo: string;
	contactsOutC: any = [];
	contactsBackupOutC: any = [];
	private outCityPage: any;
	private inCityPage: any;
	switchLang = 'inEnglish';
	limitTo: number = 700;
	
	constructor(public nav: NavController,
				public formBuilder: FormBuilder, 
				public authData: AuthData,
        		public toastCtrl: ToastController) {
		
		this.outCityPage = OutCityPage;
		this.CMN = CMN;		
		this.authData = authData;
        let self = this;
        firebase.auth().onAuthStateChanged((user) => {
            //Set different Pages link - which depends on type of user loggedin.
            if(user) {
                //returns view controller obj 
                //let view = this.nav.getActive();
        
                //prints out component name as string
                //console.log(view.component.name);
                
                //console.log("con...");
                this.getContactsInHotel(this.limitTo);
            } else {
                //Sigin page called means user must logged-out
                self.authData.logoutUser();
            }
        });
		
    }
		
	callNow(contact: any) {
		var self = this;
		CallNumber.callNumber(contact.contactnumber, true)
			.then(() => {
				//console.log('Launched dialer!');
				self.callMade(contact);
			})
			.catch(() => {
				//console.log('Error launching dialer')
			});
	}
	
	comingSoon() {
		//Displaying toast to welcome user for Login!
	    let toast = this.toastCtrl.create({
	        message: 'Coming Soon..',
	        duration: 3000,
	        position: 'bottom',
	        showCloseButton: true,
	        closeButtonText: 'OK'
	    });
	    toast.present();
	}
	
    /*Method - contacts
        fetching all contactsOutC
    */
    getContactsInHotel(limitTo: any) {
        
        var self = this;
        this.authData.getContactsInHotel(function(snapshot) {
            self.contactsOutC = [];
            let data = snapshot.val();
            
            for(let key in data) {
                let value = data[key];
                value.contactId = key;
                self.contactsOutC.push(value);
            }
            
            /* DON't RUN THIS..
            var x = 0;
			var loopArray = function(arr) {
				var value = arr[x];
				if(value.filled == undefined) {
					value.filled = 0;
				}
				
				if(value.selectHotel && value.roomNumber) {
				self.authData.getRoomStatus(function(snapshot) {
					let data = snapshot.val();
					if(data != null) {
						//#1. UPDATING ROOM STATUS (FOR PREVIOUS ASSIGNED) - TO MAINTAIN ROOM AVAILABILITY!
						uRS2(data, value, function(isSuccess) {
							x++;
							
							// any more items in array? continue loop
					        if(x < arr.length) {
					            loopArray(arr);   
					        }
						});
					} else {
						uRS1(value, function(isSuccess) {
							x++;
							
							// any more items in array? continue loop
					        if(x < arr.length) {
					            loopArray(arr);   
					        }
						});
					}
				}, value.selectHotel, value.roomNumber);
				} else {
					x++;
							
					// any more items in array? continue loop
			        if(x < arr.length) {
			            loopArray(arr);   
			        }
				}
			}
            
            loopArray(self.contactsOutC);
            
            function uRS1(value: any, callback: any) {
            	self.authData.updateRoomStatus(
					function(isUpdated) {
                        console.log("> " + isUpdated);
                        callback(true);
					},
					value.selectHotel,
					value.roomNumber,
					value.filled
				);
            }
            
            function uRS2(data: any, value: any, callback: any) {
            	//#2. Updating room status (FOR NEW ASSIGNED).
				self.authData.updateRoomStatus(
					function(isUpdated) {
						console.log("= " + isUpdated);
						callback(true);
					},
					data.hotelId,
					data.roomNumber,
					(data.filled - value.filled)
				);
            }
            */
            
            self.contactsBackupOutC = self.contactsOutC;
            
            //Resetting values
            self.searchOutCValue = '';
            
        }, limitTo);
    }
	    
	//the addContact function:
	callMade(contactTemp: any) {
		var self = this;
		
		var contact = {
			contactId:     '',
			contactnumber: '',
			totalCalls:    0
		}
		
		contact = contactTemp;
		
		contact.totalCalls = contact.totalCalls + 1;
		this.authData.callMade(
			function(isSuccess) {
				//Displaying toast for Call Started!
			    let toast = this.toastCtrl.create({
			        message: 'Call Started..!',
			        duration: 3000,
			        position: 'bottom',
			        showCloseButton: true,
			        closeButtonText: 'OK'
			    });
			    toast.present();
			},
			contact.contactId,
			contact.contactnumber,
			contact.totalCalls
		);
	}
	
	searchOutCValue = "";
	getItemsOutC(ev: any) {
		// Reset items back to all of the items
		//this.initializeItems();
		this.contactsOutC = this.contactsBackupOutC;
		
		// set val to the value of the searchbar
		let val = ev.target.value;
		this.searchOutCValue = val;
        
        // if the value is an empty string don't filter the items
		if (val && val.trim() != '') {
			this.contactsOutC = this.contactsOutC.filter((item) => {
				var filteredData = (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
                if(!filteredData && item.city) {
                    filteredData = (item.city.toLowerCase().indexOf(val.toLowerCase()) > -1);
                }
                if(!filteredData && item.arrivedBy) {
                    filteredData = (item.arrivedBy.toLowerCase().indexOf(val.toLowerCase()) > -1);
                }
                if(!filteredData && item.contactnumber) {
                    filteredData = (item.contactnumber.toString().indexOf(val.toString()) > -1);
                }
                if(!filteredData && item.selectHotel) {
                    filteredData = (CMN.hotelName[item.selectHotel].toLowerCase().indexOf(val.toLowerCase()) > -1);
                }
				return filteredData;
			})
		}
	}
	
	//we are sending the user to the EditContactPage: by User
	openEditContact(contactId: any) {
		this.nav.push(EditContactPage, {"contactId": contactId});
	}
	
	//we are sending the user to the EditContactPage: by User
	openAddContact() {
		this.nav.push(AddContactPage);
	}
	
	//we are sending the user to the EditContactPage: by User
	openReport() {
		this.nav.push(ReportPage);
	}
	
	//the addContact function:
	logoutUser() {
		this.authData.logoutUser();
	}	
}
/*

*/
@Component({
  templateUrl:  'in-city.html',
	providers: [AuthData]
})
export class InCityPage {

	CMN:      any;
	private sliderImages: Array<string>;
	contactsInC:  any = [];
	contactsBackupInC:  any = [];
	imageOrVideo: string;
	private inCityPage: any;
	private outCityPage: any;
	switchLang = 'inEnglish';
	limitTo: number = 700;
	
	constructor(public nav: NavController,
				public formBuilder: FormBuilder,
				public authData: AuthData,
				public toastCtrl: ToastController) {
		
		this.inCityPage = InCityPage;
		this.CMN = CMN;	
		this.authData = authData;
        
        let self = this;
        firebase.auth().onAuthStateChanged((user) => {
            //Set different Pages link - which depends on type of user loggedin.
            if(user) {
                //returns view controller obj 
                //let view = this.nav.getActive();
        
                //prints out component name as string
                //console.log(view.component.name);
                
                //console.log("con...");
                this.getContactsIn(this.limitTo, 'indore');
            } else {
                //Sigin page called means user must logged-out
                self.authData.logoutUser();
            }
        });

	}
		
	callNow(contact: any) {
		var self = this;
		CallNumber.callNumber(contact.contactnumber, true)
			.then(() => {
				//console.log('Launched dialer!');
				self.callMade(contact);
			})
			.catch(() => {
				//console.log('Error launching dialer')
			});
	}
	
	comingSoon() {
		//Displaying toast to welcome user for Login!
	    let toast = this.toastCtrl.create({
	        message: 'Coming Soon..',
	        duration: 3000,
	        position: 'bottom',
	        showCloseButton: true,
	        closeButtonText: 'OK'
	    });
	    toast.present();
	}
	
	//the addContact function:
	callMade(contactTemp: any) {
		var self = this;
		
		var contact = {
			contactId:     '',
			contactnumber: '',
			totalCalls:    0
		}
		
		contact = contactTemp;
		
		contact.totalCalls = contact.totalCalls + 1;
		this.authData.callMade(
			function(isSuccess) {
				//Displaying toast for Call Started!
			    let toast = this.toastCtrl.create({
			        message: 'Call Started..!',
			        duration: 3000,
			        position: 'bottom',
			        showCloseButton: true,
			        closeButtonText: 'OK'
			    });
			    toast.present();
			},
			contact.contactId,
			contact.contactnumber,
			contact.totalCalls
		);
	}
	
	searchInCValue = "";
	getItemsInC(ev: any) {
		// Reset items back to all of the items
		//this.initializeItems();
		this.contactsInC  = this.contactsBackupInC;
		
		// set val to the value of the searchbar
		let val = ev.target.value;
		this.searchInCValue = val;
        // if the value is an empty string don't filter the items
		if (val && val.trim() != '') {
			this.contactsInC = this.contactsInC.filter((item) => {
				var filteredData = (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
                if(!filteredData && item.contactnumber) {
                    filteredData = (item.contactnumber.toString().indexOf(val.toString()) > -1);
                }
                if(!filteredData && item.city) {
                    filteredData = (item.city.toLowerCase().indexOf(val.toLowerCase()) > -1);
                }
				return filteredData;
			})
		}
	}
	
	    /*Method - contacts
        fetching all contactsInC
    */
    getContactsIn(limitTo: any, city: string) {
        var self = this;
        if(self.contactsInC.length == 0) {
            this.authData.getContacts(function(snapshot) {
                self.contactsInC  = [];
                let data = snapshot.val();
                
                for(let key in data) {
                    let value = data[key];
                    value.contactId = key;
                    self.contactsInC.push(value);
                }
                
                //console.log(self.contacts.length);
                self.contactsBackupInC  = self.contactsInC;
                
                //Resetting values
                self.searchInCValue  = '';
                //console.log(data.length);
                
            }, limitTo, city);
       }
    }
		
	//we are sending the user to the EditContactPage: by User
	openReport() {
		this.nav.push(ReportPage);
	}
	
	//the addContact function:
	logoutUser() {
		this.authData.logoutUser();
	}
	
}
/*

*/
@Component({
  templateUrl:  'out-city.html',
	providers: [AuthData]
})
export class OutCityPage {

	CMN:      any;
	private sliderImages: Array<string>;
	imageOrVideo: string;
	contactsOutC: any = [];
	contactsBackupOutC: any = [];
	private outCityPage: any;
	private inCityPage: any;
	switchLang = 'inEnglish';
	limitTo: number = 700;
	
	constructor(public nav: NavController,
				public formBuilder: FormBuilder, 
				public authData: AuthData,
        		public toastCtrl: ToastController) {
		
		this.outCityPage = OutCityPage;
		this.CMN = CMN;		
		this.authData = authData;
        
        let self = this;
        firebase.auth().onAuthStateChanged((user) => {
            //Set different Pages link - which depends on type of user loggedin.
            if(user) {
                //returns view controller obj 
                //let view = this.nav.getActive();
        
                //prints out component name as string
                //console.log(view.component.name);
                
                //console.log("con...");
                this.getContactsOut(this.limitTo, '');
            } else {
                //Sigin page called means user must logged-out
                self.authData.logoutUser();
            }
        });
		
    }
		
	callNow(contact: any) {
		var self = this;
		CallNumber.callNumber(contact.contactnumber, true)
			.then(() => {
				//console.log('Launched dialer!');
				self.callMade(contact);
			})
			.catch(() => {
				//console.log('Error launching dialer')
			});
	}
	
	comingSoon() {
		//Displaying toast to welcome user for Login!
	    let toast = this.toastCtrl.create({
	        message: 'Coming Soon..',
	        duration: 3000,
	        position: 'bottom',
	        showCloseButton: true,
	        closeButtonText: 'OK'
	    });
	    toast.present();
	}
	
    /*Method - contacts
        fetching all contactsOutC
    */
    getContactsOut(limitTo: any, city: string) {
        
        var self = this;
        this.authData.getContacts(function(snapshot) {
            self.contactsOutC = [];
            let data = snapshot.val();
            
            for(let key in data) {
                let value = data[key];
                value.contactId = key;
				if(value.city != 'indore') {
                	self.contactsOutC.push(value);
				}
            }
            
            self.contactsBackupOutC = self.contactsOutC;
            
            //Resetting values
            self.searchOutCValue = '';
            
        }, limitTo, city);
    }
	    
	//the addContact function:
	callMade(contactTemp: any) {
		var self = this;
		
		var contact = {
			contactId:     '',
			contactnumber: '',
			totalCalls:    0
		}
		
		contact = contactTemp;
		
		contact.totalCalls = contact.totalCalls + 1;
		this.authData.callMade(
			function(isSuccess) {
				//Displaying toast for Call Started!
			    let toast = this.toastCtrl.create({
			        message: 'Call Started..!',
			        duration: 3000,
			        position: 'bottom',
			        showCloseButton: true,
			        closeButtonText: 'OK'
			    });
			    toast.present();
			},
			contact.contactId,
			contact.contactnumber,
			contact.totalCalls
		);
	}
	
	searchOutCValue = "";
	getItemsOutC(ev: any) {
		// Reset items back to all of the items
		//this.initializeItems();
		this.contactsOutC = this.contactsBackupOutC;
		
		// set val to the value of the searchbar
		let val = ev.target.value;
		this.searchOutCValue = val;
        
        // if the value is an empty string don't filter the items
		if (val && val.trim() != '') {
			this.contactsOutC = this.contactsOutC.filter((item) => {
				var filteredData = (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
                if(!filteredData && item.city) {
                    filteredData = (item.city.toLowerCase().indexOf(val.toLowerCase()) > -1);
                }
                if(!filteredData && item.arrivedBy) {
                    filteredData = (item.arrivedBy.toLowerCase().indexOf(val.toLowerCase()) > -1);
                }
                if(!filteredData && item.contactnumber) {
                    filteredData = (item.contactnumber.toString().indexOf(val.toString()) > -1);
                }
                if(!filteredData && item.selectHotel) {
                    filteredData = (CMN.hotelName[item.selectHotel].toLowerCase().indexOf(val.toLowerCase()) > -1);
                }
				return filteredData;
			})
		}
	}
	
	//we are sending the user to the EditContactPage: by User
	openReport() {
		this.nav.push(ReportPage);
	}
	
	//the addContact function:
	logoutUser() {
		this.authData.logoutUser();
	}
}