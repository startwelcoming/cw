/**
    Author (s): Ankit Maheshwari, 
                Pradeep Choudhary
    Date:27DEC2016
    Copyright Notice: 
    @(#)
    Description:
*/

import { Component } from '@angular/core';
import { ModalController, NavController, ToastController, LoadingController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthData } from '../../providers/auth-data/auth-data';
import { ContactsPage } from '../contacts/contacts';
import * as CMN from '../cmn/cmn';


@Component({
  templateUrl: 'edit-contact.html',
	providers: [AuthData]
})

export class EditContactPage {
	
	public event = {
		timeStarts: '07:43',
	}
	CMN:        any;
	contactId: string;
	editContactForm: FormGroup;
	contacts: any;
	roomStatusList:any = [];
	hotels:any = [];
	hotelRooms:any = [];
	
	contact = {
		hotelName: "",
		selectHotel: 0,
		arrivedBy:   "",
		arrivalDetail:  "",
		arrivalTime: Date.parse(""),
		roomNumber:  "",
		checkInOut:  "",
		bookingForDay: {
			1: false,
			2: false
		},
		filled: null,
		remark: ""
	};
	
	constructor(public modalCtrl: ModalController, public nav: NavController,
				public authData: AuthData,         public formBuilder: FormBuilder,
				public navParams: NavParams, public toastCtrl: ToastController, 
                public loadingCtrl: LoadingController) {

		this.CMN = CMN;
		this.nav = nav;
		this.authData = authData;
		this.contactId = navParams.get('contactId');
		
		this.editContactForm = formBuilder.group({
			selectHotel:             ['', Validators.required],
			roomNumber:              ['', Validators.required],
			arrivedBy:               ['', Validators.required],
			arrivalDetail:           ['', Validators.required],
			arrivalTime:             ['', Validators.required],
			checkInOut:              ['', Validators.required],
			bookingForDay1:          ['', Validators.required],
			bookingForDay2:          ['', Validators.required],
			filled:                  ['', Validators.required],
			remark:                  ['', Validators.required],
		})
		
		var self = this;
		this.authData.getContactInHotel(function(snapshot) {
			let dataContact = snapshot.val();
			
			//Fetching Current Room Status
			self.authData.getRoomStatusList(function(snapshot) {
				let dataRoomStatusList = snapshot.val();
				if(dataRoomStatusList) {
					self.roomStatusList = dataRoomStatusList;
					
					//SET CONTACT DETAILS..
					self.contacts = dataContact;
					
					//Pre-setting values..
					self.contact.hotelName       = self.contacts.hotelName;
					self.contact.selectHotel     = self.contacts.selectHotel;
					self.contact.arrivedBy       = self.contacts.arrivedBy;
					self.contact.arrivalDetail   = self.contacts.arrivalDetail ? self.contacts.arrivalDetail : '';
					self.contact.arrivalTime     = self.contacts.arrivalTime;
					self.contact.roomNumber      = self.contacts.roomNumber ? self.contacts.roomNumber : '';
					self.contact.checkInOut      = self.contacts.checkInOut ? self.contacts.checkInOut : '';
					self.contact.bookingForDay   = self.contacts.bookingForDay ? self.contacts.bookingForDay : {1: false, 2: false};
					self.contact.filled          = self.contacts.filled ? self.contacts.filled : null;
					self.contact.remark          = self.contacts.remark ? self.contacts.remark : '';
				}
				self.hotels         = self.CMN.hotels;
				self.hotelRooms     = self.CMN.hotelRooms;
			});
			
		}, this.contactId);
	}
	
	//the addContact function:
	editContact(event) {
		var self = this;
		
		this.editContactForm.value.filled 
			= this.editContactForm.value.filled ? this.editContactForm.value.filled : self.contact.filled;
		
		this.editContactForm.value.arrivalDetail 
			= this.editContactForm.value.arrivalDetail ? this.editContactForm.value.arrivalDetail : self.contact.arrivalDetail;
		
		this.editContactForm.value.remark 
			= this.editContactForm.value.remark ? this.editContactForm.value.remark : self.contact.remark;
		
		this.editContactForm.value.checkInOut 
			= this.editContactForm.value.checkInOut ? this.editContactForm.value.checkInOut : self.contact.checkInOut;
		
		if(this.editContactForm.value.selectHotel != undefined) {
			event.preventDefault();
			
            //Display loader
            var loading = self.loadingCtrl.create({
                content: "Loading..."
            });
            loading.present();
            
			//Fetching current status of user - before update
			var dataContact = self.contacts;
			self.authData.editContactInHotel(
				function(isSuccess) {
					//Information updated successfully!
					//self.nav.pop();
					if(dataContact) {
						//#1. Checking if user is shifted to another HOTEL & ROOM
						if((dataContact.selectHotel != undefined) && (self.contact.selectHotel != dataContact.selectHotel)) {
							//fetching current room status.
							self.authData.getRoomStatus(function(snapshot) {
								let data = snapshot.val();
								if(data != null) {
									//#1. UPDATING ROOM STATUS (FOR PREVIOUS ASSIGNED) - TO MAINTAIN ROOM AVAILABILITY!
									self.authData.updateRoomStatus(
										function(isUpdated) {
											//#2. Updating room status (FOR NEW ASSIGNED).
											self.authData.updateRoomStatus(
												function(isUpdated) {
                                                    loading.dismiss();
                                                    self.nav.setRoot(ContactsPage);
												},
												data.hotelId,
												data.roomNumber,
												(data.filled - self.contact.filled)
											);
										},
										self.editContactForm.value.selectHotel,
										self.editContactForm.value.roomNumber,
										self.editContactForm.value.filled
									);
								}
							}, dataContact.selectHotel, dataContact.roomNumber);
						}
						//#2. Checking if user is shifted to another ROOM
						else if((dataContact.selectHotel != undefined) &&
							(self.contact.selectHotel == dataContact.selectHotel) && 
							(self.contact.roomNumber != dataContact.roomNumber)) {
							
							//fetching current room status.
							self.authData.getRoomStatus(function(snapshot) {
								let data = snapshot.val();
								if(data != null) {
									//#1. UPDATING ROOM STATUS (FOR PREVIOUS ASSIGNED) - TO MAINTAIN ROOM AVAILABILITY!
									self.authData.updateRoomStatus(
										function(isUpdated) {
											//#2. Updating room status (FOR NEW ASSIGNED).
											self.authData.updateRoomStatus(
												function(isUpdated) {
                                                    loading.dismiss();
                                                    self.nav.setRoot(ContactsPage);
												},
												data.hotelId,
												data.roomNumber,
												(data.filled - self.contact.filled)
											);
										},
										self.editContactForm.value.selectHotel,
										self.editContactForm.value.roomNumber,
										self.editContactForm.value.filled
									);
								}
							}, dataContact.selectHotel, dataContact.roomNumber);
						} else {
							//#3. UPDATING ROOM STATUS (FOR PREVIOUS ASSIGNED) - TO MAINTAIN ROOM AVAILABILITY!
							self.authData.updateRoomStatus(
								function(isUpdated) {
                                    loading.dismiss();
                                    self.nav.setRoot(ContactsPage);
								},
								self.editContactForm.value.selectHotel,
								self.editContactForm.value.roomNumber,
								self.editContactForm.value.filled
							);
						}
					} else {
                        //#3. UPDATING ROOM STATUS (FOR PREVIOUS ASSIGNED) - TO MAINTAIN ROOM AVAILABILITY!
                        self.authData.updateRoomStatus(
                            function(isUpdated) {
                                loading.dismiss();
                                self.nav.setRoot(ContactsPage);
                            },
                            self.editContactForm.value.selectHotel,
                            self.editContactForm.value.roomNumber,
                            self.editContactForm.value.filled
                        );
                    }
					
					/*
					CMN.hotelRooms[self.editContactForm.value.selectHotel].filter(function( obj ) {
						//return obj.roomNo == self.contact.roomNumber;
						if(obj.roomNo == self.contact.roomNumber) {
							//UPDATING ROOM STATUS - TO MAINTAIN ROOM AVAILABILITY!
							self.authData.updateRoomStatus(
								self.editContactForm.value.selectHotel,
								self.contact.roomNumber,
								self.contact.capacity
							);
						} else {
							alert("Something went wrong!");
						}
					});
					*/
				},
				self.contactId,
				self.editContactForm.value.selectHotel,
				self.editContactForm.value.arrivedBy ? self.editContactForm.value.arrivedBy : '',
				self.editContactForm.value.arrivalDetail,
				self.editContactForm.value.arrivalTime ? self.editContactForm.value.arrivalTime : '',
				self.editContactForm.value.roomNumber ? self.editContactForm.value.roomNumber : '',
				self.editContactForm.value.checkInOut,
				{
					1: self.editContactForm.value.bookingForDay1,
					2: self.editContactForm.value.bookingForDay2
				},
				self.editContactForm.value.filled,
				self.editContactForm.value.remark
			);
		} else {
            //Displaying toast to welcome user!
            let toast = self.toastCtrl.create({
                message: 'Please select Hotel, Room No.',
                duration: 3000,
                position: 'bottom',
                showCloseButton: true,
                closeButtonText: 'OK'
            });
            toast.present();
		}
	}
}