/**
    Author (s): Ankit Maheshwari, 
                Pradeep Choudhary
    Date:27DEC2016
    Copyright Notice: 
    @(#)
    Description:
*/

import { Component } from '@angular/core';
import { ModalController, NavController, Toast } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthData } from '../../providers/auth-data/auth-data';
import * as CMN from '../cmn/cmn';


@Component({
  templateUrl: 'hotel.html',
	providers: [AuthData]
})

export class HotelPage {
	
	public event = {
		timeStarts: '07:43',
	}
	CMN: any;
	addHotelForm: FormGroup;
	
	constructor(public modalCtrl: ModalController, public nav: NavController,
		public authData: AuthData, public formBuilder: FormBuilder) {

		this.CMN = CMN;
		this.nav = nav;
		this.authData = authData;

		this.addHotelForm = formBuilder.group({
			address:       ['', Validators.required],
			hotelName:     ['', Validators.required],
			city:          ['', Validators.required],
		})
	}
	//the addContact function:
	addHotelInfo(event) {
		event.preventDefault();

		this.authData.addHotelInfo(this.addHotelForm.value.hotelName,      
									 this.addHotelForm.value.address,   
									 this.addHotelForm.value.city
								);
	}

}