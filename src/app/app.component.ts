import { Component, ViewChild } from '@angular/core';

import { StatusBar, Splashscreen } from 'ionic-native';

import { Platform, MenuController, Nav } from 'ionic-angular';
import { HomePage } from '../pages/home/home';
import { SignInPage } from '../pages/sign-in/sign-in';
import { SignUpPage } from '../pages/sign-up/sign-up';
import { AddContactPage } from '../pages/add-contact/add-contact';
import { HotelPage } from '../pages/hotel/hotel';
import { ContactsPage } from '../pages/contacts/contacts';
import { InCityPage } from '../pages/contacts/contacts';
import { OutCityPage } from '../pages/contacts/contacts';
import { InHotelPage } from '../pages/contacts/contacts';
import { EditContactPage } from '../pages/edit-contact/edit-contact';
import { LogOutPage } from '../pages/log-out/log-out';


import * as CMN from '../pages/cmn/cmn';

//import * as firebase from "firebase";
declare var firebase: any;

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
	@ViewChild(Nav) nav: Nav;
	
	//make HomePage the root (or first) page
	rootPage: any = ContactsPage;
	
	isLoggedIn: boolean = false;
	
	// make HomePage the root (or first) page
	rootPageDefault:     any = SignInPage;
	pagesDefault:        any;
	pagesBottomDefault:  any;
	
	// make HomePage the root (or first) page
	rootPageAdmin:     any = ContactsPage;
	pagesAdmin:        any;
	pagesBottomAdmin:  any;
	
	CMN: any;  
	defaultPages: any; 
	adminPages:  any;
	
	defaultPagesBottom: any; 
	adminPagesBottom:  any;
	
  constructor(
    public platform: Platform,
    public menu: MenuController
  ) {
	  	var self = this;
        
        //this.initializeApp();
        this.platform.ready().then(() => {
			this.hideSplashScreen();
		});
        
	        // Initialize Firebase
	        var config = {
	            apiKey:        "AIzaSyCd8vzxVvf4qCJdlqBm7AwwsirT5Lkpc48",
	            authDomain:    "cw-app-e38d5.firebaseapp.com",
	            databaseURL:   "https://cw-app-e38d5.firebaseio.com",
				storageBucket: "cw-app-e38d5.appspot.com"
	        };
	        firebase.initializeApp(config);
	        
			this.CMN = CMN;
		
		    // set our app's pages
		    this.pagesDefault = [
				//{ icon: 'person-add',  title: CMN.menuList.link12, component: SignUpPage, 
				  //categAliasName: 'Signup'
				//}
		    ];
		  
		     // set our app's pages
		    this.pagesBottomDefault = [
		      //{ icon: 'appstore',      title: 'Copyrights',      component: CopyRightsPage },
		    ];
		  
		    // set our app's pages
		    this.pagesAdmin = [
				//{ icon: 'people',     title: CMN.menuListC.link3, component: ContactsPage},
				{ icon: 'add-circle', title: 'Add Contact', component: AddContactPage},
				//{ icon: 'star',     title: CMN.menuListC.link2, component: HotelPage},
				{ icon: 'log-out',    title: 'Log Out', component: LogOutPage},
			];
		  
		     // set our app's pages
		    this.pagesBottomAdmin = [
		      
		    ];
			
			//1. When no one loggedin.
			self.rootPageDefault = SignInPage;
			
			//2. When user loggedin.
			self.rootPageAdmin   = ContactsPage;
			
	        //an observer for the user object. By using an observer, 
	        //you ensure that the Auth object isn�t in an intermediate 
	        //state�such as initialization�when you get the current user. 
	        firebase.auth().onAuthStateChanged((user) => {
				//Set different Pages link - which depends on type of user loggedin.
	            if(user) {
	                //If there's a user take him to the home page.
				    //3. When admin loggedin.
					self.isLoggedIn = true;
					
					//returns view controller obj 
					//let view = this.nav.getActive();
					
					//prints out component name as string
					//console.log(view.component.name);
					//This check is required to avoid double loading of RootPage.
					//if(view.component.name != 'HomePage') {
						//2. When user loggedin.
					self.rootPage = self.rootPageAdmin;
					//}
	            } else {
	                //If there's no user logged in send him to the LoginPage
					//1. When no one loggedin.
					self.isLoggedIn = false;
					//2. When user loggedin.
					self.rootPage = SignInPage;
	            }
	        });
    }
  	
  	
  	hideSplashScreen() {
		if (Splashscreen) {
			setTimeout(() => {
				Splashscreen.hide();
			}, 100);
		}
	};
  	
  	initializeApp() {
		this.platform.ready().then(() => {
			this.hideSplashScreen();
		});
	}
	
	openPage(page) {
		//returns view controller obj 
		//let view = this.nav.getActive();
		
		//prints out component name as string
		//console.log(view.component.name);
		//console.log(page.component.name);
		
		// navigate to the new page if it is not the current page
		this.nav.push(page.component, {categAliasName: page.categAliasName});
		
		// close the menu when clicking a link from the menu
		this.menu.close();
		
	}
}
