import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ReportPage } from '../pages/report/report';
import { SignInPage } from '../pages/sign-in/sign-in';
import { SignUpPage } from '../pages/sign-up/sign-up';
import { AddContactPage } from '../pages/add-contact/add-contact';
import { HotelPage } from '../pages/hotel/hotel';
import { InCityPage } from '../pages/contacts/contacts';
import { OutCityPage } from '../pages/contacts/contacts';
import { ContactsPage } from '../pages/contacts/contacts';
import { InHotelPage } from '../pages/contacts/contacts';
import { EditContactPage } from '../pages/edit-contact/edit-contact';
import { LogOutPage } from '../pages/log-out/log-out';
import { RoomPage } from '../pages/room/room';

@NgModule({
  declarations: [
    MyApp,
	HomePage,
	ReportPage,
	SignInPage,
	SignUpPage,
	AddContactPage,
	HotelPage,
	ContactsPage,
	InCityPage,
	OutCityPage,
	InHotelPage,
	EditContactPage,
	LogOutPage,
	RoomPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
	HomePage,
	ReportPage,
	SignInPage,
	SignUpPage,
	AddContactPage,
	HotelPage,
	ContactsPage,
	InCityPage,
	OutCityPage,
	InHotelPage,
	EditContactPage,
	LogOutPage,
	RoomPage
  ],
  providers: []
})
export class AppModule {}
