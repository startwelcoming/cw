import { Component, ViewChild } from '@angular/core';

import { Platform, MenuController, Nav } from 'ionic-angular';

import { StatusBar } from 'ionic-native';

import { HomePage } from '../pages/home/home';
import { NewsDetailPage } from '../pages/news-detail/news-detail';
import { NewsCategoryPage } from '../pages/news-category/news-category';
import { NewsVideosPage } from '../pages/news-videos/news-videos';
import { NewsGalleryPage } from '../pages/news-gallery/news-gallery';
import { AboutUsPage } from '../pages/about-us/about-us';
import { NewsTopPage } from '../pages/news-top/news-top';
import { ShareAppPage } from '../pages/share-app/share-app';
import * as CMN from '../pages/cmn/cmn';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  // make HomePage the root (or first) page
  rootPage: any = HomePage;
	
  CMN: any;  
  defaultPages: any; 
  userPages:    any; 
  sellerPages:  any;
  pages:        Array<{icon: string, title: string, component: any}>;
  
  defaultPagesBottom: any; 
  userPagesBottom:    any; 
  sellerPagesBottom:  any;
  pagesBottom:        Array<{icon: string, title: string, component: any}>;

  constructor(
    public platform: Platform,
    public menu: MenuController
  ) {
	  
	this.CMN = CMN;
    this.initializeApp();

    // set our app's pages
    this.defaultPages = [
		{ icon: 'home',					title: CMN.menuList.link1,	component: HomePage, 
		  categAliasName: 'home'
		},
		{ icon: 'videocam',				title: CMN.menuList.link2,	component: NewsVideosPage, 
		  categAliasName: 'videocam'
		},
		{ icon: 'camera',				title: CMN.menuList.link3,	component: NewsGalleryPage, 
		  categAliasName: 'camera'
		},
		{ icon: 'radio-button-on',		title: CMN.menuList.link4,	component: NewsTopPage, 
		  categAliasName: 'country'
		},
		{ icon: 'radio-button-on',		title: CMN.menuList.link5,	component: NewsTopPage, 
		  categAliasName: 'world'
		},
		{ icon: 'radio-button-on',		title: CMN.menuList.link6,	component: NewsTopPage, 
		  categAliasName: 'sports'
		},
		{ icon: 'radio-button-on',		title: CMN.menuList.link7,	component: NewsTopPage, 
		  categAliasName: 'entertainment'
		},
		{ icon: 'radio-button-on',		title: CMN.menuList.link8,	component: NewsTopPage, 
		  categAliasName: 'lifestyle'
		},
		{ icon: 'radio-button-on',		title: CMN.menuList.link9,	component: NewsTopPage, 
		  categAliasName: 'technology'
		},
		{ icon: 'radio-button-on',		title: CMN.menuList.link10,	component: NewsTopPage, 
		  categAliasName: 'gadgets'
		},
		{ icon: 'radio-button-on',		title: CMN.menuList.link11,	component: NewsTopPage, 
		  categAliasName: 'automobile'
		},
		{ icon: 'radio-button-on',		title: CMN.menuList.link12,	component: NewsTopPage, 
		  categAliasName: 'crime'
		},
     
    ];
  
     // set our app's pages
    this.defaultPagesBottom = [
     //{ icon: 'appstore',        title: 'About VC',        component: AboutUsPage },
      //{ icon: 'appstore',      title: 'Copyrights',      component: CopyRightsPage },
    ];
      
    // set our app's pages
    this.userPages = [
      //{ icon: 'grid',          title: 'Market Grid',     component: MarketGridPage },
      //{ icon: 'chatboxes',     title: 'Hello Ionic',     component: HelloIonicPage },
      //{ icon: 'logo-snapchat', title: 'My First List',   component: ListPage },
      //{ icon: 'appstore',      title: 'About Us',        component: UserAboutPage },
      //{ icon: 'chatboxes',     title: 'Shop Messages',   component: UserSellerMessagesPage },
      //{ icon: 'log-in',        title: 'Login',           component: UserSigninSignupPage },
    ];
  
     // set our app's pages
    this.userPagesBottom = [
      
    ];
    
    // set our app's pages
    this.sellerPages = [
      //{ icon: 'home',          title: 'Home',          component: ShopClothingMarketHomePage },
      //{ icon: 'appstore',      title: 'About Us',      component: SellerAboutPage },
      //{ icon: 'megaphone',     title: 'User Queries',  component: SellerUserQueriesPage },
      //{ icon: 'people',        title: 'User Group',    component: UserGroupPage },
     // { icon: 'log-in',        title: 'Login',         component: UserSigninSignupPage },
    ];
  
     // set our app's pages
    this.sellerPagesBottom = [
      
    ];
    
    //Set different Pages link - which depends on type of user loggedin.
    //1. When no one loggedin.
    this.pages       = this.defaultPages;
    this.pagesBottom = this.defaultPagesBottom;
    
    //2. When user loggedin.
    //this.pages       = this.userPages;
    //this.pagesBottom = this.userPagesBottom;
    
    //3. When seller loggedin.
    //this.pages       = this.sellerPages;
    //this.pagesBottom = this.sellerPagesBottom;
    
    }
  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
    });
  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page.component, {categAliasName: page.categAliasName});
  }
}
