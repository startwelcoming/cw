import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { NewsDetailPage } from '../pages/news-detail/news-detail';
import { NewsCategoryPage } from '../pages/news-category/news-category';
import { NewsVideosPage } from '../pages/news-videos/news-videos';
import { NewsGalleryPage } from '../pages/news-gallery/news-gallery';
import { CitySelectModal } from '../pages/city-select-modal/city-select-modal';
import { LangSelectModal } from '../pages/lang-select-modal/lang-select-modal';
import { AboutUsPage } from '../pages/about-us/about-us';
import { NewsTopPage } from '../pages/news-top/news-top';
import { ShareAppPage } from '../pages/share-app/share-app';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
	NewsDetailPage,
	NewsCategoryPage,
	NewsVideosPage,
	NewsGalleryPage,
	CitySelectModal,
    LangSelectModal,
    AboutUsPage,
	NewsTopPage,
    ShareAppPage
   
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
	NewsDetailPage,
	NewsCategoryPage,
	NewsVideosPage,
	NewsGalleryPage,
	CitySelectModal,
    LangSelectModal,
    AboutUsPage,
	NewsTopPage,
    ShareAppPage
  
  ],
  providers: []
})
export class AppModule {}
