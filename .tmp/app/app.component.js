import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav } from 'ionic-angular';
import { StatusBar } from 'ionic-native';
import { HomePage } from '../pages/home/home';
import { NewsVideosPage } from '../pages/news-videos/news-videos';
import { NewsGalleryPage } from '../pages/news-gallery/news-gallery';
import { NewsTopPage } from '../pages/news-top/news-top';
import * as CMN from '../pages/cmn/cmn';
export var MyApp = (function () {
    function MyApp(platform, menu) {
        this.platform = platform;
        this.menu = menu;
        // make HomePage the root (or first) page
        this.rootPage = HomePage;
        this.CMN = CMN;
        this.initializeApp();
        // set our app's pages
        this.defaultPages = [
            { icon: 'home', title: CMN.menuList.link1, component: HomePage,
                categAliasName: 'home'
            },
            { icon: 'videocam', title: CMN.menuList.link2, component: NewsVideosPage,
                categAliasName: 'videocam'
            },
            { icon: 'camera', title: CMN.menuList.link3, component: NewsGalleryPage,
                categAliasName: 'camera'
            },
            { icon: 'radio-button-on', title: CMN.menuList.link4, component: NewsTopPage,
                categAliasName: 'country'
            },
            { icon: 'radio-button-on', title: CMN.menuList.link5, component: NewsTopPage,
                categAliasName: 'world'
            },
            { icon: 'radio-button-on', title: CMN.menuList.link6, component: NewsTopPage,
                categAliasName: 'sports'
            },
            { icon: 'radio-button-on', title: CMN.menuList.link7, component: NewsTopPage,
                categAliasName: 'entertainment'
            },
            { icon: 'radio-button-on', title: CMN.menuList.link8, component: NewsTopPage,
                categAliasName: 'lifestyle'
            },
            { icon: 'radio-button-on', title: CMN.menuList.link9, component: NewsTopPage,
                categAliasName: 'technology'
            },
            { icon: 'radio-button-on', title: CMN.menuList.link10, component: NewsTopPage,
                categAliasName: 'gadgets'
            },
            { icon: 'radio-button-on', title: CMN.menuList.link11, component: NewsTopPage,
                categAliasName: 'automobile'
            },
            { icon: 'radio-button-on', title: CMN.menuList.link12, component: NewsTopPage,
                categAliasName: 'crime'
            },
        ];
        // set our app's pages
        this.defaultPagesBottom = [];
        // set our app's pages
        this.userPages = [];
        // set our app's pages
        this.userPagesBottom = [];
        // set our app's pages
        this.sellerPages = [];
        // set our app's pages
        this.sellerPagesBottom = [];
        //Set different Pages link - which depends on type of user loggedin.
        //1. When no one loggedin.
        this.pages = this.defaultPages;
        this.pagesBottom = this.defaultPagesBottom;
        //2. When user loggedin.
        //this.pages       = this.userPages;
        //this.pagesBottom = this.userPagesBottom;
        //3. When seller loggedin.
        //this.pages       = this.sellerPages;
        //this.pagesBottom = this.sellerPagesBottom;
    }
    MyApp.prototype.initializeApp = function () {
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            StatusBar.styleDefault();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // close the menu when clicking a link from the menu
        this.menu.close();
        // navigate to the new page if it is not the current page
        this.nav.setRoot(page.component, { categAliasName: page.categAliasName });
    };
    MyApp.decorators = [
        { type: Component, args: [{
                    templateUrl: 'app.html'
                },] },
    ];
    /** @nocollapse */
    MyApp.ctorParameters = [
        { type: Platform, },
        { type: MenuController, },
    ];
    MyApp.propDecorators = {
        'nav': [{ type: ViewChild, args: [Nav,] },],
    };
    return MyApp;
}());
