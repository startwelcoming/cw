import { Platform, MenuController, Nav } from 'ionic-angular';
export declare class MyApp {
    platform: Platform;
    menu: MenuController;
    nav: Nav;
    rootPage: any;
    CMN: any;
    defaultPages: any;
    userPages: any;
    sellerPages: any;
    pages: Array<{
        icon: string;
        title: string;
        component: any;
    }>;
    defaultPagesBottom: any;
    userPagesBottom: any;
    sellerPagesBottom: any;
    pagesBottom: Array<{
        icon: string;
        title: string;
        component: any;
    }>;
    constructor(platform: Platform, menu: MenuController);
    initializeApp(): void;
    openPage(page: any): void;
}
