import { ModalController } from 'ionic-angular';
export declare var cmnLabelList: {
    "BY": {
        ENG: string;
        HINDI: string;
    };
};
export declare var selLang: string;
export declare var selCity: string;
export declare var langLabel: {
    ENG: string;
    HINDI: string;
};
export declare var langList: {
    label: string;
    value: string;
}[];
export declare var stateList: {
    "MP": {
        ENG: string;
        HINDI: string;
    };
};
export declare var cityLabelList: {
    "INDORE": {
        ENG: string;
        HINDI: string;
    };
    "BHOPAL": {
        ENG: string;
        HINDI: string;
    };
    "JABALPUR": {
        ENG: string;
        HINDI: string;
    };
    "UJJAIN": {
        ENG: string;
        HINDI: string;
    };
    "GWALIOR": {
        ENG: string;
        HINDI: string;
    };
};
export declare var cityStateList: {
    state: {
        ENG: string;
        HINDI: string;
    };
    cities: {
        key: string;
        value: {
            ENG: string;
            HINDI: string;
        };
    }[];
}[];
export declare var btnLabelList: {
    langBtn: {
        "ENG": string;
        "HINDI": string;
    };
    homeBtn: {
        "ENG": string;
        "HINDI": string;
    };
    cityBtn: {
        "ENG": string;
        "HINDI": string;
    };
};
export declare var menuList: {
    link1: {
        "ENG": string;
        "HINDI": string;
    };
    link2: {
        "ENG": string;
        "HINDI": string;
    };
    link3: {
        "ENG": string;
        "HINDI": string;
    };
    link4: {
        "ENG": string;
        "HINDI": string;
    };
    link5: {
        "ENG": string;
        "HINDI": string;
    };
    link6: {
        "ENG": string;
        "HINDI": string;
    };
    link7: {
        "ENG": string;
        "HINDI": string;
    };
    link8: {
        "ENG": string;
        "HINDI": string;
    };
    link9: {
        "ENG": string;
        "HINDI": string;
    };
    link10: {
        "ENG": string;
        "HINDI": string;
    };
    link11: {
        "ENG": string;
        "HINDI": string;
    };
    link12: {
        "ENG": string;
        "HINDI": string;
    };
};
export declare var homeTabList: {
    tab1: {
        "ENG": string;
        "HINDI": string;
    };
    tab2: {
        "ENG": string;
        "HINDI": string;
    };
    tab3: {
        "ENG": string;
        "HINDI": string;
    };
    tab4: {
        "ENG": string;
        "HINDI": string;
    };
};
export declare var newsTopHeaderLabelList: {
    label1: {
        "ENG": string;
        "HINDI": string;
    };
    label2: {
        "ENG": string;
        "HINDI": string;
    };
    label3: {
        "ENG": string;
        "HINDI": string;
    };
    label4: {
        "ENG": string;
        "HINDI": string;
    };
    label5: {
        "ENG": string;
        "HINDI": string;
    };
    label6: {
        "ENG": string;
        "HINDI": string;
    };
    label7: {
        "ENG": string;
        "HINDI": string;
    };
    label8: {
        "ENG": string;
        "HINDI": string;
    };
    label9: {
        "ENG": string;
        "HINDI": string;
    };
    label10: {
        "ENG": string;
        "HINDI": string;
    };
    label11: {
        "ENG": string;
        "HINDI": string;
    };
    label12: {
        "ENG": string;
        "HINDI": string;
    };
    label13: {
        "ENG": string;
        "HINDI": string;
    };
};
export declare var newsBottomHeaderLabelList: {
    label1: {
        "ENG": string;
        "HINDI": string;
    };
    label2: {
        "ENG": string;
        "HINDI": string;
    };
    label3: {
        "ENG": string;
        "HINDI": string;
    };
    label4: {
        "ENG": string;
        "HINDI": string;
    };
    label5: {
        "ENG": string;
        "HINDI": string;
    };
    label6: {
        "ENG": string;
        "HINDI": string;
    };
    label7: {
        "ENG": string;
        "HINDI": string;
    };
    label8: {
        "ENG": string;
        "HINDI": string;
    };
    label9: {
        "ENG": string;
        "HINDI": string;
    };
    label10: {
        "ENG": string;
        "HINDI": string;
    };
    label11: {
        "ENG": string;
        "HINDI": string;
    };
    label12: {
        "ENG": string;
        "HINDI": string;
    };
    label13: {
        "ENG": string;
        "HINDI": string;
    };
};
export declare var labelList: {
    headerTitle: {
        "ENG": string;
        "HINDI": string;
    };
};
export declare var detailList: {
    listHeader: {
        "ENG": string;
        "HINDI": string;
    };
};
export declare var newsTopLabelList: {
    label1: {
        "ENG": string;
        "HINDI": string;
    };
    label2: {
        "ENG": string;
        "HINDI": string;
    };
};
export declare function updateSelLang(newValue: string): void;
export declare function updateCityLang(newValue: string): void;
export declare function showLangSelectModal(modalCtrl: ModalController): void;
export declare function showCitySelectModal(modalCtrl: ModalController): void;
export declare class CMNClass {
    constructor();
}
