import { LangSelectModal } from '../../pages/lang-select-modal/lang-select-modal';
import { CitySelectModal } from '../../pages/city-select-modal/city-select-modal';
'use strict';
//Global variable, accessible throughout the app.
export var cmnLabelList = {
    "BY": {
        ENG: "by",
        HINDI: "के द्वारा"
    },
};
//Default language is ENG - English
export var selLang = 'ENG';
//Default language is ENG - English
export var selCity = 'INDORE';
export var langLabel = {
    ENG: "English",
    HINDI: "हिन्दी",
};
//langList for language select modal
export var langList = [
    {
        label: langLabel["ENG"],
        value: "ENG"
    },
    {
        label: langLabel["HINDI"],
        value: "HINDI"
    }
];
//cityList for city select modal
export var stateList = {
    "MP": {
        ENG: "Madhya Pradesh",
        HINDI: "मध्य प्रदेश"
    }
};
export var cityLabelList = {
    "INDORE": {
        ENG: "Indore",
        HINDI: "इंदौर"
    },
    "BHOPAL": {
        ENG: "Bhopal",
        HINDI: "भोपाल"
    },
    "JABALPUR": {
        ENG: "jabalpur",
        HINDI: "जबलपुर"
    },
    "UJJAIN": {
        ENG: "Ujjain",
        HINDI: "उज्जैन"
    },
    "GWALIOR": {
        ENG: "Gwalior",
        HINDI: "ग्वालियर"
    }
};
//cityList for city select modal
export var cityStateList = [
    {
        state: stateList["MP"],
        cities: [
            {
                key: "INDORE",
                value: cityLabelList["INDORE"]
            },
            {
                key: "BHOPAL",
                value: cityLabelList["BHOPAL"]
            },
            {
                key: "JABALPUR",
                value: cityLabelList["JABALPUR"]
            },
            {
                key: "UJJAIN",
                value: cityLabelList["UJJAIN"]
            },
            {
                key: "GWALIOR",
                value: cityLabelList["GWALIOR"]
            }
        ]
    }
];
//btnLabelList for Title, buttons etc..
export var btnLabelList = {
    langBtn: {
        "ENG": langLabel["ENG"],
        "HINDI": langLabel["HINDI"]
    },
    homeBtn: {
        "ENG": "Home",
        "HINDI": "होम"
    },
    cityBtn: {
        "ENG": "City",
        "HINDI": "शहर"
    }
};
//menu list of home .
export var menuList = {
    link1: {
        "ENG": "Home",
        "HINDI": "होम"
    },
    link2: {
        "ENG": "Videos",
        "HINDI": "वीडियो"
    },
    link3: {
        "ENG": "Gallery",
        "HINDI": "गैलरी"
    },
    link4: {
        "ENG": "Country",
        "HINDI": "देश"
    },
    link5: {
        "ENG": "World",
        "HINDI": "दुनिया"
    },
    link6: {
        "ENG": "Sports",
        "HINDI": "खेल"
    },
    link7: {
        "ENG": "Entertainment",
        "HINDI": "मनोरंजन"
    },
    link8: {
        "ENG": "Lifestyle",
        "HINDI": "लाइफस्टाइल"
    },
    link9: {
        "ENG": "Technology",
        "HINDI": "टेक्नोलॉजी"
    },
    link10: {
        "ENG": "Gadgets",
        "HINDI": "गैजेटों"
    },
    link11: {
        "ENG": "Automobile",
        "HINDI": "ऑटोमोबाइल"
    },
    link12: {
        "ENG": "Crime",
        "HINDI": "क्राइम"
    }
};
//tab list of home .
export var homeTabList = {
    tab1: {
        "ENG": "National",
        "HINDI": "राष्ट्रीय"
    },
    tab2: {
        "ENG": "State",
        "HINDI": "राज्य"
    },
    tab3: {
        "ENG": "City",
        "HINDI": "शहर"
    },
    tab4: {
        "ENG": "Village",
        "HINDI": "ग्रामीण"
    }
};
//tab list of home .
export var newsTopHeaderLabelList = {
    label1: {
        "ENG": "Top National News",
        "HINDI": "शीर्ष राष्ट्रीय समाचार"
    },
    label2: {
        "ENG": "Top State News",
        "HINDI": "शीर्ष राज्य समाचार"
    },
    label3: {
        "ENG": "Top City News",
        "HINDI": "शीर्ष शहर समाचार"
    },
    label4: {
        "ENG": "Top Village News",
        "HINDI": "शीर्ष ग्रामीण समाचार"
    },
    label5: {
        "ENG": "Top Country News",
        "HINDI": "शीर्ष देश समाचार"
    },
    label6: {
        "ENG": "Top World News",
        "HINDI": "शीर्ष दुनिया समाचार"
    },
    label7: {
        "ENG": "Top Sports News",
        "HINDI": "शीर्ष खेल समाचार"
    },
    label8: {
        "ENG": "Top Entertainment News",
        "HINDI": "शीर्ष मनोरंजन समाचार"
    },
    label9: {
        "ENG": "Top Lifestyle News",
        "HINDI": "शीर्ष लाइफस्टाइल समाचार"
    },
    label10: {
        "ENG": "Top Technology News",
        "HINDI": "शीर्ष टेक्नोलॉजी समाचार"
    },
    label11: {
        "ENG": "Top Gadgets News",
        "HINDI": "शीर्ष गैजेटों समाचार"
    },
    label12: {
        "ENG": "Top Automobile News",
        "HINDI": "शीर्ष ऑटोमोबाइल समाचार"
    },
    label13: {
        "ENG": "Top Crime News",
        "HINDI": "शीर्ष क्राइम समाचार"
    }
};
//tab list of home .
export var newsBottomHeaderLabelList = {
    label1: {
        "ENG": "All From National",
        "HINDI": "सभी राष्ट्रीय से"
    },
    label2: {
        "ENG": "All From State",
        "HINDI": "सभी राज्य से"
    },
    label3: {
        "ENG": "All From City",
        "HINDI": "सभी सिटी से"
    },
    label4: {
        "ENG": "All From Village",
        "HINDI": "सभी ग्रामीण से"
    },
    label5: {
        "ENG": "All From Country",
        "HINDI": "सभी देश से"
    },
    label6: {
        "ENG": "All From World",
        "HINDI": "सभी दुनिया से"
    },
    label7: {
        "ENG": "All From Sports",
        "HINDI": "सभी खेल से"
    },
    label8: {
        "ENG": "All From Entertainment",
        "HINDI": "सभी मनोरंजन से"
    },
    label9: {
        "ENG": "All From Lifestyle",
        "HINDI": "सभी लाइफस्टाइल से"
    },
    label10: {
        "ENG": "All From Technology",
        "HINDI": "सभी टेक्नोलॉजी से"
    },
    label11: {
        "ENG": "All From Gadgets",
        "HINDI": "सभी गैजेटों से"
    },
    label12: {
        "ENG": "All From Automobile",
        "HINDI": "सभी ऑटोमोबाइल से"
    },
    label13: {
        "ENG": "All From Crime",
        "HINDI": "सभी क्राइम से"
    }
};
//labelList for Title, buttons etc..
export var labelList = {
    headerTitle: {
        "ENG": "Top News",
        "HINDI": "मुख्य समाचार"
    }
};
//detailList for listHeader
export var detailList = {
    listHeader: {
        "ENG": "Related News",
        "HINDI": "संबंधित समाचार"
    }
};
//newsTopLabelList for Title, buttons etc..
export var newsTopLabelList = {
    label1: {
        "ENG": "Home",
        "HINDI": "होम"
    },
    label2: {
        "ENG": "City",
        "HINDI": "शहर"
    }
};
export function updateSelLang(newValue) {
    selLang = newValue;
}
export function updateCityLang(newValue) {
    selCity = newValue;
}
export function showLangSelectModal(modalCtrl) {
    var modal = modalCtrl.create(LangSelectModal);
    modal.present();
}
export function showCitySelectModal(modalCtrl) {
    var modal = modalCtrl.create(CitySelectModal);
    modal.present();
}
export var CMNClass = (function () {
    function CMNClass() {
    }
    return CMNClass;
}());
