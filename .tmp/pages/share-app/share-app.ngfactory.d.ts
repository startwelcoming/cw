import * as import1 from '@angular/core/src/linker/view';
import * as import2 from '@angular/core/src/linker/element';
import * as import3 from './share-app';
import * as import4 from '@angular/core/src/linker/view_utils';
import * as import5 from '@angular/core/src/di/injector';
import * as import9 from '@angular/core/src/linker/component_factory';
export declare const ShareAppPageNgFactory: import9.ComponentFactory<import3.ShareAppPage>;
export declare function viewFactory_ShareAppPage0(viewUtils: import4.ViewUtils, parentInjector: import5.Injector, declarationEl: import2.AppElement): import1.AppView<import3.ShareAppPage>;
