import { Component } from '@angular/core';
import { ModalController, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { NewsDetailPage } from '../news-detail/news-detail';
import { NewsCategoryPage } from '../news-category/news-category';
import * as CMN from '../cmn/cmn';
export var NewsTopPage = (function () {
    function NewsTopPage(modalCtrl, nav, navParams) {
        this.modalCtrl = modalCtrl;
        this.nav = nav;
        this.navParams = navParams;
        this.CMN = CMN;
        this.categAliasName = navParams.get('categAliasName');
        this.initializeItems();
    }
    NewsTopPage.prototype.initializeItems = function () {
        switch (this.categAliasName) {
            case "national":
                this.newsTopHeaderLabel = CMN.newsTopHeaderLabelList.label1;
                this.newsBottomHeaderLabel = CMN.newsBottomHeaderLabelList.label1;
                break;
            case "state":
                this.newsTopHeaderLabel = CMN.newsTopHeaderLabelList.label2;
                this.newsBottomHeaderLabel = CMN.newsBottomHeaderLabelList.label2;
                break;
            case "city":
                this.newsTopHeaderLabel = CMN.newsTopHeaderLabelList.label3;
                this.newsBottomHeaderLabel = CMN.newsBottomHeaderLabelList.label3;
                break;
            case "village":
                this.newsTopHeaderLabel = CMN.newsTopHeaderLabelList.label4;
                this.newsBottomHeaderLabel = CMN.newsBottomHeaderLabelList.label4;
                break;
            case "country":
                this.newsTopHeaderLabel = CMN.newsTopHeaderLabelList.label5;
                this.newsBottomHeaderLabel = CMN.newsBottomHeaderLabelList.label5;
                break;
            case "world":
                this.newsTopHeaderLabel = CMN.newsTopHeaderLabelList.label6;
                this.newsBottomHeaderLabel = CMN.newsBottomHeaderLabelList.label6;
                break;
            case "sports":
                this.newsTopHeaderLabel = CMN.newsTopHeaderLabelList.label7;
                this.newsBottomHeaderLabel = CMN.newsBottomHeaderLabelList.label7;
                break;
            case "entertainment":
                this.newsTopHeaderLabel = CMN.newsTopHeaderLabelList.label8;
                this.newsBottomHeaderLabel = CMN.newsBottomHeaderLabelList.label8;
                break;
            case "lifestyle":
                this.newsTopHeaderLabel = CMN.newsTopHeaderLabelList.label9;
                this.newsBottomHeaderLabel = CMN.newsBottomHeaderLabelList.label9;
                break;
            case "technology":
                this.newsTopHeaderLabel = CMN.newsTopHeaderLabelList.label10;
                this.newsBottomHeaderLabel = CMN.newsBottomHeaderLabelList.label10;
                break;
            case "gadgets":
                this.newsTopHeaderLabel = CMN.newsTopHeaderLabelList.label11;
                this.newsBottomHeaderLabel = CMN.newsBottomHeaderLabelList.label11;
                break;
            case "automobile":
                this.newsTopHeaderLabel = CMN.newsTopHeaderLabelList.label12;
                this.newsBottomHeaderLabel = CMN.newsBottomHeaderLabelList.label12;
                break;
            case "crime":
                this.newsTopHeaderLabel = CMN.newsTopHeaderLabelList.label13;
                this.newsBottomHeaderLabel = CMN.newsBottomHeaderLabelList.label13;
                break;
        }
        //Displaying the News List - data store structure!
        this.newsList = [
            {
                title: {
                    ENG: "Polluted Ganga in DelhiPolluted",
                    HINDI: "दिल्ली में प्रदूषित गंगा"
                },
                correspondentName: {
                    ENG: "Sushil Sharma",
                    HINDI: "सुशील शर्मा"
                },
                description: {
                    ENG: "The most popular industrial group ever, and largely responsible for bringing the music to a mass audience. and largely responsible for bringing the music to a mass audience.",
                    HINDI: "सबसे लोकप्रिय औद्योगिक समूह कभी, और एक व्यापक दर्शकों के लिए संगीत लाने के लिए काफी हद तक जिम्मेदार है। और एक व्यापक दर्शकों के लिए संगीत लाने के लिए काफी हद तक जिम्मेदार"
                },
                mediaType: "image",
                mediaSrc: "img/czz.jpg",
                tags: "",
                state: {
                    ENG: "M.P.",
                    HINDI: "एम.पी."
                },
                city: {
                    ENG: "Indore",
                    HINDI: "इंदौर"
                },
                correspondentId: "",
                createDate: "12/12/2016",
                updateDate: ""
            }
        ];
        //Displaying the impNewsList - data store structure!
        this.impNewsList = [
            {
                title: {
                    ENG: "The most popular  the music to a mass audience. and largely responsible for bringing the music to a mass audience.",
                    HINDI: "सबसे लोकप्रिय औद्योगिक समूह कभी  और एक व्यापक दर्शकों के लिए संगीत लाने के लिए काफी हद तक जिम्मेदार"
                },
                mediaType: "image",
                mediaSrc: "img/marty-avatar.png",
            },
            {
                title: {
                    ENG: "The most popular  the music to a mass audience. and largely responsible for bringing the music to a mass audience.",
                    HINDI: "सबसे लोकप्रिय औद्योगिक समूह कभी  और एक व्यापक दर्शकों के लिए संगीत लाने के लिए काफी हद तक जिम्मेदार"
                },
                mediaType: "image",
                mediaSrc: "img/marty-avatar.png",
            }
        ];
        //Displaying the News List - data store structure!
        this.retNewsList = [
            {
                title: {
                    ENG: "The most popular  the music to a mass audience. and largely responsible for bringing the music to a mass audience.",
                    HINDI: "सबसे लोकप्रिय औद्योगिक समूह कभी  और एक व्यापक दर्शकों के लिए संगीत लाने के लिए काफी हद तक जिम्मेदार"
                },
                mediaType: "image",
                mediaSrc: "img/marty-avatar.png",
            }
        ];
    };
    NewsTopPage.prototype.showLangSelectModal = function () {
        CMN.showLangSelectModal(this.modalCtrl);
    };
    //we are sending the user to the langSelectModal: by User
    NewsTopPage.prototype.showCitySelectModal = function () {
        CMN.showCitySelectModal(this.modalCtrl);
    };
    //we are sending the user to the HomePage: by User
    NewsTopPage.prototype.openHome = function () {
        this.nav.setRoot(HomePage);
    };
    //we are sending the user to the NewsCategoryPage: by User
    NewsTopPage.prototype.openNewsCategory = function () {
        this.nav.setRoot(NewsCategoryPage, { categAliasName: this.categAliasName });
    };
    //we are sending the user to the NewsDetailPage: by User
    NewsTopPage.prototype.openNewsDetail = function () {
        this.nav.setRoot(NewsDetailPage);
    };
    //we are sending the user to the NewsTopPage: by User
    NewsTopPage.prototype.openNewsTop = function (categAliasName) {
        this.nav.setRoot(NewsTopPage, { categAliasName: categAliasName });
    };
    NewsTopPage.decorators = [
        { type: Component, args: [{
                    templateUrl: 'news-top.html'
                },] },
    ];
    /** @nocollapse */
    NewsTopPage.ctorParameters = [
        { type: ModalController, },
        { type: NavController, },
        { type: NavParams, },
    ];
    return NewsTopPage;
}());
