import * as import1 from '@angular/core/src/linker/view';
import * as import2 from '@angular/core/src/linker/element';
import * as import3 from './news-top';
import * as import4 from '@angular/core/src/linker/view_utils';
import * as import5 from '@angular/core/src/di/injector';
import * as import12 from '@angular/core/src/linker/component_factory';
export declare const NewsTopPageNgFactory: import12.ComponentFactory<import3.NewsTopPage>;
export declare function viewFactory_NewsTopPage0(viewUtils: import4.ViewUtils, parentInjector: import5.Injector, declarationEl: import2.AppElement): import1.AppView<import3.NewsTopPage>;
