import { ModalController, NavController, NavParams } from 'ionic-angular';
export declare class NewsTopPage {
    modalCtrl: ModalController;
    nav: NavController;
    navParams: NavParams;
    CMN: any;
    newsList: any;
    impNewsList: any;
    retNewsList: any;
    categAliasName: string;
    newsTopHeaderLabel: any;
    newsBottomHeaderLabel: any;
    constructor(modalCtrl: ModalController, nav: NavController, navParams: NavParams);
    initializeItems(): void;
    showLangSelectModal(): void;
    showCitySelectModal(): void;
    openHome(): void;
    openNewsCategory(): void;
    openNewsDetail(): void;
    openNewsTop(categAliasName: any): void;
}
