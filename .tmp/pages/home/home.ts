import { Component } from '@angular/core';
import { ModalController, Platform, NavParams, ViewController, NavController} from 'ionic-angular';
import { NewsDetailPage } from '../news-detail/news-detail';
import { NewsTopPage } from '../news-top/news-top';
import * as CMN from '../cmn/cmn';

@Component({
    templateUrl: 'home.html'
})
export class HomePage {
	
	CMN:      any;
    newsList: any;
	mahakalTitle: any;
	mahakalSlides: any;
	
	constructor(public modalCtrl: ModalController, public nav: NavController) {
		
		this.nav = nav;
		this.CMN = CMN;
		this.initializeItems();
    }

	initializeItems() {
		
		//Displaying the slides - data store structure!
		this.mahakalTitle = {
				ENG: "MAHAKAL DARSHAN",
				HINDI: "महाकाल दर्शन"
		},
		this.mahakalSlides = [
			{
				subTitle:
				{
					ENG: "MORNING 5:30",
					HINDI: "सुबह 5:30 बजे "
				},
				image: "img/men.jpg",
			},
			{
				subTitle:
				{
					ENG: "MORNING 5:30",
					HINDI: "सुबह 5:30 बजे"
				},
				image: "img/czz.jpg",
			}
		];
			
		//Displaying the News List - data store structure!
		this.newsList = [
			{
				title:
					{
						ENG: "Polluted Ganga in DelhiPolluted",
						HINDI: "दिल्ली में प्रदूषित गंगा"
					},
				correspondentName:
					{
						ENG: "Sushil Sharma",
						HINDI: "सुशील शर्मा"
					},
				description:
					{
						ENG: "The most popular industrial group ever, and largely responsible for bringing the music to a mass audience. and largely responsible for bringing the music to a mass audience.",
						HINDI: "सबसे लोकप्रिय औद्योगिक समूह कभी, और एक व्यापक दर्शकों के लिए संगीत लाने के लिए काफी हद तक जिम्मेदार है। और एक व्यापक दर्शकों के लिए संगीत लाने के लिए काफी हद तक जिम्मेदार"
					},
				mediaType: "image", //image or video
				mediaSrc: "img/czz.jpg",
				tags: "",
				state:
					{
						ENG: "M.P.",
						HINDI: "एम.पी."
					},
				city:
					{
						ENG: "Indore",
						HINDI: "इंदौर"
					},
				correspondentId: "",
				createDate: "12/12/2016",
				updateDate: ""
			},
			{
				title:
					{
						ENG: "Polluted Ganga in DelhiPolluted",
						HINDI: "दिल्ली में प्रदूषित गंगा"
					},
				correspondentName:
					{
						ENG: "Sushil Sharma",
						HINDI: "सुशील शर्मा"
					},
				description:
					{
						ENG: "The most popular industrial group ever, and largely responsible for bringing the music to a mass audience. and largely responsible for bringing the music to a mass audience.",
						HINDI: "सबसे लोकप्रिय औद्योगिक समूह कभी, और एक व्यापक दर्शकों के लिए संगीत लाने के लिए काफी हद तक जिम्मेदार है। और एक व्यापक दर्शकों के लिए संगीत लाने के लिए काफी हद तक जिम्मेदार"
					},
				mediaType: "video", //image or video
				mediaSrc: "http://player.vimeo.com/external/85569724.sd.mp4?s=43df5df0d733011263687d20a47557e4",
				tags: "",
				state:
					{
						ENG: "M.P.",
						HINDI: "एम.पी."
					},
				city:
					{
						ENG: "Indore",
						HINDI: "इंदौर"
					},
				correspondentId: "",
				createDate: "12/12/2016",
				updateDate: ""
			}
		];
		
    }
	
	mahakalSlideOptions = {
		initialSlide: 0,
		autoplay: 8000,
		loop: true,
		pager: true,
	};	
	
	//we are sending the user to the NewsDetailPage: by User
    openNewsDetail() {
        this.nav.setRoot(NewsDetailPage);
    }
		
	//we are sending the user to the NewsTopPage: by User
    openNewsTop(categAliasName) {
        this.nav.setRoot(NewsTopPage, {categAliasName: categAliasName});
    }
	
	//we are sending the user to the langSelectModal: by User
	showLangSelectModal() {
		CMN.showLangSelectModal(this.modalCtrl);
	}
	
	//we are sending the user to the langSelectModal: by User
	showCitySelectModal() {
		CMN.showCitySelectModal(this.modalCtrl);
	}
}