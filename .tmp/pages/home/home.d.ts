import { ModalController, NavController } from 'ionic-angular';
export declare class HomePage {
    modalCtrl: ModalController;
    nav: NavController;
    CMN: any;
    newsList: any;
    mahakalTitle: any;
    mahakalSlides: any;
    constructor(modalCtrl: ModalController, nav: NavController);
    initializeItems(): void;
    mahakalSlideOptions: {
        initialSlide: number;
        autoplay: number;
        loop: boolean;
        pager: boolean;
    };
    openNewsDetail(): void;
    openNewsTop(categAliasName: any): void;
    showLangSelectModal(): void;
    showCitySelectModal(): void;
}
