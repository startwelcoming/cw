import { Component } from '@angular/core';
import { ModalController, Platform, NavParams, ViewController, NavController} from 'ionic-angular';
import { NewsDetailPage } from '../news-detail/news-detail';
import { NewsTopPage } from '../news-top/news-top';
import { HomePage } from '../home/home';
import * as CMN from '../cmn/cmn';

@Component({
    templateUrl: 'news-gallery.html'
})
export class NewsGalleryPage {
	
	CMN: any;
    newsList: any;
	
	constructor(public modalCtrl: ModalController, public nav: NavController) {
		
		this.nav = nav;
		this.CMN = CMN;
		this.initializeItems();
    }

    initializeItems() {
        //Displaying the News List - data store structure!
        this.newsList = [
            {
                title:
                {
                    ENG: "Polluted Ganga in DelhiPolluted",
                    HINDI: "दिल्ली में प्रदूषित गंगा"
                },
                description:
                {
                    ENG: "The most popular industrial group ever, and largely responsible for bringing the music to a mass audience. and largely responsible for bringing the music to a mass audience.",
                    HINDI: "सबसे लोकप्रिय औद्योगिक समूह कभी और एक व्यापक दर्शकों के लिए संगीत लाने के लिए काफी हद तक जिम्मेदार है। और एक व्यापक दर्शकों के लिए संगीत लाने के लिए काफी हद तक जिम्मेदार है।",
				},
                mediaType: "image", //image or video
                mediaSrc: "img/czz.jpg",
                tags: "",
                state:
                {
                    ENG: "M.P.",
                    HINDI: "एम.पी.।"
                },
                city:
                {
                    ENG: "Indore",
                    HINDI: "इंदौर"
                },
                correspondentId: "",
                createDate: "12/12/2016",
                updateDate: ""
            },
            {
                title:
                {
                    ENG: "Polluted Ganga in DelhiPolluted",
                    HINDI: "दिल्ली में प्रदूषित गंगा"
                },
                description:
                {
                    ENG: "The most popular industrial group ever, and largely responsible for bringing the music to a mass audience. and largely responsible for bringing the music to a mass audience.",
                    HINDI: "सबसे लोकप्रिय औद्योगिक समूह कभी और एक व्यापक दर्शकों के लिए संगीत लाने के लिए काफी हद तक जिम्मेदार है। और एक व्यापक दर्शकों के लिए संगीत लाने के लिए काफी हद तक जिम्मेदार है।",
				},
                mediaType: "image", //image or video
                mediaSrc: "img/czz.jpg",
                tags: "",
                state:
                {
                    ENG: "M.P.",
                    HINDI: "एम.पी.।"
                },
                city:
                {
                    ENG: "Indore",
                    HINDI: "इंदौर"
                },
                correspondentId: "",
                createDate: "12/12/2016",
                updateDate: ""
            },
        ];
    }
	
	//we are sending the user to the NewsDetailPage: by User
    openNewsDetail() {
        this.nav.setRoot(NewsDetailPage);
    }
	//we are sending the user to the HomePage: by User
    openHome() {
        this.nav.setRoot(HomePage);
    }
	
	showLangSelectModal() {
		CMN.showLangSelectModal(this.modalCtrl);
	}
	
	//we are sending the user to the langSelectModal: by User
	showCitySelectModal() {
		CMN.showCitySelectModal(this.modalCtrl);
	}
	
	//we are sending the user to the NewsTopPage: by User
    openNewsTop(categAliasName) {
        this.nav.setRoot(NewsTopPage, {categAliasName: categAliasName});
    }
}