import { ModalController, NavController } from 'ionic-angular';
export declare class NewsGalleryPage {
    modalCtrl: ModalController;
    nav: NavController;
    CMN: any;
    newsList: any;
    constructor(modalCtrl: ModalController, nav: NavController);
    initializeItems(): void;
    openNewsDetail(): void;
    openHome(): void;
    showLangSelectModal(): void;
    showCitySelectModal(): void;
    openNewsTop(categAliasName: any): void;
}
