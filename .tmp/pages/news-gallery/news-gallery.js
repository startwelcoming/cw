import { Component } from '@angular/core';
import { ModalController, NavController } from 'ionic-angular';
import { NewsDetailPage } from '../news-detail/news-detail';
import { NewsTopPage } from '../news-top/news-top';
import { HomePage } from '../home/home';
import * as CMN from '../cmn/cmn';
export var NewsGalleryPage = (function () {
    function NewsGalleryPage(modalCtrl, nav) {
        this.modalCtrl = modalCtrl;
        this.nav = nav;
        this.nav = nav;
        this.CMN = CMN;
        this.initializeItems();
    }
    NewsGalleryPage.prototype.initializeItems = function () {
        //Displaying the News List - data store structure!
        this.newsList = [
            {
                title: {
                    ENG: "Polluted Ganga in DelhiPolluted",
                    HINDI: "दिल्ली में प्रदूषित गंगा"
                },
                description: {
                    ENG: "The most popular industrial group ever, and largely responsible for bringing the music to a mass audience. and largely responsible for bringing the music to a mass audience.",
                    HINDI: "सबसे लोकप्रिय औद्योगिक समूह कभी और एक व्यापक दर्शकों के लिए संगीत लाने के लिए काफी हद तक जिम्मेदार है। और एक व्यापक दर्शकों के लिए संगीत लाने के लिए काफी हद तक जिम्मेदार है।",
                },
                mediaType: "image",
                mediaSrc: "img/czz.jpg",
                tags: "",
                state: {
                    ENG: "M.P.",
                    HINDI: "एम.पी.।"
                },
                city: {
                    ENG: "Indore",
                    HINDI: "इंदौर"
                },
                correspondentId: "",
                createDate: "12/12/2016",
                updateDate: ""
            },
            {
                title: {
                    ENG: "Polluted Ganga in DelhiPolluted",
                    HINDI: "दिल्ली में प्रदूषित गंगा"
                },
                description: {
                    ENG: "The most popular industrial group ever, and largely responsible for bringing the music to a mass audience. and largely responsible for bringing the music to a mass audience.",
                    HINDI: "सबसे लोकप्रिय औद्योगिक समूह कभी और एक व्यापक दर्शकों के लिए संगीत लाने के लिए काफी हद तक जिम्मेदार है। और एक व्यापक दर्शकों के लिए संगीत लाने के लिए काफी हद तक जिम्मेदार है।",
                },
                mediaType: "image",
                mediaSrc: "img/czz.jpg",
                tags: "",
                state: {
                    ENG: "M.P.",
                    HINDI: "एम.पी.।"
                },
                city: {
                    ENG: "Indore",
                    HINDI: "इंदौर"
                },
                correspondentId: "",
                createDate: "12/12/2016",
                updateDate: ""
            },
        ];
    };
    //we are sending the user to the NewsDetailPage: by User
    NewsGalleryPage.prototype.openNewsDetail = function () {
        this.nav.setRoot(NewsDetailPage);
    };
    //we are sending the user to the HomePage: by User
    NewsGalleryPage.prototype.openHome = function () {
        this.nav.setRoot(HomePage);
    };
    NewsGalleryPage.prototype.showLangSelectModal = function () {
        CMN.showLangSelectModal(this.modalCtrl);
    };
    //we are sending the user to the langSelectModal: by User
    NewsGalleryPage.prototype.showCitySelectModal = function () {
        CMN.showCitySelectModal(this.modalCtrl);
    };
    //we are sending the user to the NewsTopPage: by User
    NewsGalleryPage.prototype.openNewsTop = function (categAliasName) {
        this.nav.setRoot(NewsTopPage, { categAliasName: categAliasName });
    };
    NewsGalleryPage.decorators = [
        { type: Component, args: [{
                    templateUrl: 'news-gallery.html'
                },] },
    ];
    /** @nocollapse */
    NewsGalleryPage.ctorParameters = [
        { type: ModalController, },
        { type: NavController, },
    ];
    return NewsGalleryPage;
}());
