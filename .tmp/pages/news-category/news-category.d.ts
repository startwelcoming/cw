import { ModalController, NavController, NavParams } from 'ionic-angular';
export declare class NewsCategoryPage {
    modalCtrl: ModalController;
    nav: NavController;
    navParams: NavParams;
    CMN: any;
    retNewsList: any;
    categAliasName: string;
    newsBottomHeaderLabel: any;
    constructor(modalCtrl: ModalController, nav: NavController, navParams: NavParams);
    initializeItems(): void;
    showLangSelectModal(): void;
    showCitySelectModal(): void;
    openHome(): void;
    openNewsDetail(): void;
    openNewsTop(categAliasName: any): void;
}
