import { Component } from '@angular/core';
import { ModalController, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { NewsDetailPage } from '../news-detail/news-detail';
import { NewsTopPage } from '../news-top/news-top';
import * as CMN from '../cmn/cmn';
export var NewsCategoryPage = (function () {
    function NewsCategoryPage(modalCtrl, nav, navParams) {
        this.modalCtrl = modalCtrl;
        this.nav = nav;
        this.navParams = navParams;
        this.CMN = CMN;
        this.categAliasName = navParams.get('categAliasName');
        this.initializeItems();
    }
    NewsCategoryPage.prototype.initializeItems = function () {
        switch (this.categAliasName) {
            case "national":
                this.newsBottomHeaderLabel = CMN.newsBottomHeaderLabelList.label1;
                break;
            case "state":
                this.newsBottomHeaderLabel = CMN.newsBottomHeaderLabelList.label2;
                break;
            case "city":
                this.newsBottomHeaderLabel = CMN.newsBottomHeaderLabelList.label3;
                break;
            case "village":
                this.newsBottomHeaderLabel = CMN.newsBottomHeaderLabelList.label4;
                break;
            case "country":
                this.newsBottomHeaderLabel = CMN.newsBottomHeaderLabelList.label5;
                break;
            case "world":
                this.newsBottomHeaderLabel = CMN.newsBottomHeaderLabelList.label6;
                break;
            case "sports":
                this.newsBottomHeaderLabel = CMN.newsBottomHeaderLabelList.label7;
                break;
            case "entertainment":
                this.newsBottomHeaderLabel = CMN.newsBottomHeaderLabelList.label8;
                break;
            case "lifestyle":
                this.newsBottomHeaderLabel = CMN.newsBottomHeaderLabelList.label9;
                break;
            case "technology":
                this.newsBottomHeaderLabel = CMN.newsBottomHeaderLabelList.label10;
                break;
            case "gadgets":
                this.newsBottomHeaderLabel = CMN.newsBottomHeaderLabelList.label11;
                break;
            case "automobile":
                this.newsBottomHeaderLabel = CMN.newsBottomHeaderLabelList.label12;
                break;
            case "crime":
                this.newsBottomHeaderLabel = CMN.newsBottomHeaderLabelList.label13;
                break;
        }
        //Displaying the News List - data store structure!
        this.retNewsList = [
            {
                title: {
                    ENG: "The most popular  the music to a mass audience. and largely responsible for bringing the music to a mass audience.",
                    HINDI: "सबसे लोकप्रिय औद्योगिक समूह कभी  और एक व्यापक दर्शकों के लिए संगीत लाने के लिए काफी हद तक जिम्मेदार"
                },
                mediaType: "image",
                mediaSrc: "img/marty-avatar.png",
            },
            {
                title: {
                    ENG: "The most popular  the music to a mass audience. and largely responsible for bringing the music to a mass audience.",
                    HINDI: "सबसे लोकप्रिय औद्योगिक समूह कभी  और एक व्यापक दर्शकों के लिए संगीत लाने के लिए काफी हद तक जिम्मेदार"
                },
                mediaType: "image",
                mediaSrc: "img/marty-avatar.png",
            }
        ];
    };
    NewsCategoryPage.prototype.showLangSelectModal = function () {
        CMN.showLangSelectModal(this.modalCtrl);
    };
    //we are sending the user to the langSelectModal: by User
    NewsCategoryPage.prototype.showCitySelectModal = function () {
        CMN.showCitySelectModal(this.modalCtrl);
    };
    //we are sending the user to the HomePage: by User
    NewsCategoryPage.prototype.openHome = function () {
        this.nav.setRoot(HomePage);
    };
    //we are sending the user to the NewsDetailPage: by User
    NewsCategoryPage.prototype.openNewsDetail = function () {
        this.nav.setRoot(NewsDetailPage);
    };
    //we are sending the user to the NewsTopPage: by User
    NewsCategoryPage.prototype.openNewsTop = function (categAliasName) {
        this.nav.setRoot(NewsTopPage, { categAliasName: categAliasName });
    };
    NewsCategoryPage.decorators = [
        { type: Component, args: [{
                    templateUrl: 'news-category.html'
                },] },
    ];
    /** @nocollapse */
    NewsCategoryPage.ctorParameters = [
        { type: ModalController, },
        { type: NavController, },
        { type: NavParams, },
    ];
    return NewsCategoryPage;
}());
