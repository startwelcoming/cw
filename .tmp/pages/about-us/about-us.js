import { Component } from '@angular/core';
export var AboutUsPage = (function () {
    function AboutUsPage() {
    }
    AboutUsPage.decorators = [
        { type: Component, args: [{
                    templateUrl: 'about-us.html'
                },] },
    ];
    /** @nocollapse */
    AboutUsPage.ctorParameters = [];
    return AboutUsPage;
}());
