import { ModalController, NavController } from 'ionic-angular';
export declare class NewsVideosPage {
    modalCtrl: ModalController;
    nav: NavController;
    CMN: any;
    newsList: any;
    constructor(modalCtrl: ModalController, nav: NavController);
    initializeItems(): void;
    openNewsDetail(): void;
    openHome(): void;
    showLangSelectModal(): void;
    openNewsTop(categAliasName: any): void;
    showCitySelectModal(): void;
}
