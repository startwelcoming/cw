import { Component } from '@angular/core';
import { Platform, NavParams, ViewController } from 'ionic-angular';
import * as CMN from '../cmn/cmn';
export var CitySelectModal = (function () {
    function CitySelectModal(viewCtrl, platform, navParams) {
        this.viewCtrl = viewCtrl;
        this.platform = platform;
        this.navParams = navParams;
        this.CMN = CMN;
        this.initializeItems();
    }
    CitySelectModal.prototype.initializeItems = function () {
        //Displaying the News List - data store structure!
        this.newSelCity = CMN.selCity;
    };
    //This function is responsible to change the language of the App.
    CitySelectModal.prototype.changeCity = function (newSelCity) {
        CMN.updateCityLang(newSelCity);
        this.viewCtrl.dismiss();
    };
    CitySelectModal.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    CitySelectModal.decorators = [
        { type: Component, args: [{
                    templateUrl: 'city-select-modal.html'
                },] },
    ];
    /** @nocollapse */
    CitySelectModal.ctorParameters = [
        { type: ViewController, },
        { type: Platform, },
        { type: NavParams, },
    ];
    return CitySelectModal;
}());
