import { Platform, NavParams, ViewController } from 'ionic-angular';
export declare class CitySelectModal {
    viewCtrl: ViewController;
    platform: Platform;
    navParams: NavParams;
    CMN: any;
    newSelCity: any;
    constructor(viewCtrl: ViewController, platform: Platform, navParams: NavParams);
    initializeItems(): void;
    changeCity(newSelCity: any): void;
    close(): void;
}
