import { Component } from '@angular/core';
import { ModalController, Platform, NavParams, ViewController } from 'ionic-angular';
import * as CMN from '../cmn/cmn';

@Component({
    templateUrl: 'city-select-modal.html'
})
export class CitySelectModal {

	CMN:        any;
	newSelCity: any;

	constructor(public viewCtrl: ViewController, public platform: Platform,
		public navParams: NavParams) {
		
		this.CMN = CMN;
		this.initializeItems();
	}

	initializeItems() {
		//Displaying the News List - data store structure!
		this.newSelCity = CMN.selCity;
	}

	//This function is responsible to change the language of the App.
	changeCity(newSelCity) {
		CMN.updateCityLang(newSelCity);
		this.viewCtrl.dismiss();
	}
	
	close() {
		this.viewCtrl.dismiss();
	}
}