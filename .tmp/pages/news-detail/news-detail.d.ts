import { ModalController, NavController } from 'ionic-angular';
export declare class NewsDetailPage {
    modalCtrl: ModalController;
    nav: NavController;
    CMN: any;
    detaiNewsList: any;
    retNewsList: any;
    constructor(modalCtrl: ModalController, nav: NavController);
    initializeItems(): void;
    showLangSelectModal(): void;
    showCitySelectModal(): void;
    openHome(): void;
    openNewsTop(categAliasName: any): void;
}
