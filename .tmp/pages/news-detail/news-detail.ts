import { Component } from '@angular/core';
import { ModalController, NavController} from 'ionic-angular';
import { HomePage } from '../home/home';
import { NewsTopPage } from '../news-top/news-top';
import * as CMN from '../cmn/cmn';

@Component({
	templateUrl: 'news-detail.html'
})
export class NewsDetailPage {
	
	CMN: any;
	detaiNewsList: any;
	retNewsList:   any;
	constructor(public modalCtrl: ModalController, public nav: NavController) {
		this.CMN = CMN;
		this.initializeItems();
	}
	
	initializeItems() {
		//Displaying the News List - data store structure!
		this.detaiNewsList = [
			{
                title:
                {
                    ENG: "Polluted Ganga in DelhiPolluted",
                    HINDI: "दिल्ली में प्रदूषित गंगा"
                },
				correspondentName:
                {
                    ENG: "Sushil Sharma",
                    HINDI: "सुशील शर्मा"
                },
                description:
                {
                    ENG: "The most popular industrial group ever, and largely responsible for bringing the music to a mass audience. and largely responsible for bringing the music to a mass audience.",
                    HINDI: "सबसे लोकप्रिय औद्योगिक समूह कभी, और एक व्यापक दर्शकों के लिए संगीत लाने के लिए काफी हद तक जिम्मेदार है। और एक व्यापक दर्शकों के लिए संगीत लाने के लिए काफी हद तक जिम्मेदार"
                },
                mediaType: "image", //image or video
                mediaSrc: "img/czz.jpg",
                tags: "",
                state:
                {
                    ENG: "M.P.",
                    HINDI: "एम.पी."
                },
                city:
                {
                    ENG: "Indore",
                    HINDI: "इंदौर"
                },
                correspondentId: "",
                createDate: "12/12/2016",
                updateDate: "",
			}
		];
		
		//Displaying the News List - data store structure!
        this.retNewsList = [
            {
                title:
                {
                    ENG: "The most popular  the music to a mass audience. and largely responsible for bringing the music to a mass audience.",
                    HINDI: "सबसे लोकप्रिय औद्योगिक समूह कभी  और एक व्यापक दर्शकों के लिए संगीत लाने के लिए काफी हद तक जिम्मेदार"
                },
                mediaType: "image", //image or video
                mediaSrc: "img/marty-avatar.png",
            },
			{
                title:
                {
                    ENG: "The most popular  the music to a mass audience. and largely responsible for bringing the music to a mass audience.",
                    HINDI: "सबसे लोकप्रिय औद्योगिक समूह कभी  और एक व्यापक दर्शकों के लिए संगीत लाने के लिए काफी हद तक जिम्मेदार"
                },
                mediaType: "image", //image or video
                mediaSrc: "img/marty-avatar.png",
            }
        ];
	}
	
	showLangSelectModal() {
		CMN.showLangSelectModal(this.modalCtrl);
	}
	
	//we are sending the user to the langSelectModal: by User
	showCitySelectModal() {
		CMN.showCitySelectModal(this.modalCtrl);
	}	
	
	//we are sending the user to the HomePage: by User
	openHome() {
		this.nav.setRoot(HomePage);
	}
			
	//we are sending the user to the NewsTopPage: by User
	openNewsTop(categAliasName) {
		this.nav.setRoot(NewsTopPage, {categAliasName: categAliasName});
	}
}