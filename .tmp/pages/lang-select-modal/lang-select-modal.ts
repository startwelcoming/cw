import { Component } from '@angular/core';
import { ModalController, Platform, NavParams, ViewController } from 'ionic-angular';
import * as CMN from '../cmn/cmn';

@Component({
    templateUrl: 'lang-select-modal.html'
})
export class LangSelectModal {

	CMN:        any;
	langList:   any;
	newSelLang: any;

	constructor(public viewCtrl: ViewController, public platform: Platform,
		public navParams: NavParams) {
		
		this.CMN = CMN;
		this.initializeItems();
	}

	initializeItems() {
		//Displaying the News List - data store structure!
		this.langList   = CMN.langList;
		this.newSelLang = CMN.selLang;
	}

	//This function is responsible to change the language of the App.
	changeLang(newSelLang) {
		CMN.updateSelLang(newSelLang);
		this.viewCtrl.dismiss();
	}
	
	close() {
		this.viewCtrl.dismiss();
	}
}