import { Platform, NavParams, ViewController } from 'ionic-angular';
export declare class LangSelectModal {
    viewCtrl: ViewController;
    platform: Platform;
    navParams: NavParams;
    CMN: any;
    langList: any;
    newSelLang: any;
    constructor(viewCtrl: ViewController, platform: Platform, navParams: NavParams);
    initializeItems(): void;
    changeLang(newSelLang: any): void;
    close(): void;
}
