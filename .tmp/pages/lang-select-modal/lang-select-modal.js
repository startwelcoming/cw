import { Component } from '@angular/core';
import { Platform, NavParams, ViewController } from 'ionic-angular';
import * as CMN from '../cmn/cmn';
export var LangSelectModal = (function () {
    function LangSelectModal(viewCtrl, platform, navParams) {
        this.viewCtrl = viewCtrl;
        this.platform = platform;
        this.navParams = navParams;
        this.CMN = CMN;
        this.initializeItems();
    }
    LangSelectModal.prototype.initializeItems = function () {
        //Displaying the News List - data store structure!
        this.langList = CMN.langList;
        this.newSelLang = CMN.selLang;
    };
    //This function is responsible to change the language of the App.
    LangSelectModal.prototype.changeLang = function (newSelLang) {
        CMN.updateSelLang(newSelLang);
        this.viewCtrl.dismiss();
    };
    LangSelectModal.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    LangSelectModal.decorators = [
        { type: Component, args: [{
                    templateUrl: 'lang-select-modal.html'
                },] },
    ];
    /** @nocollapse */
    LangSelectModal.ctorParameters = [
        { type: ViewController, },
        { type: Platform, },
        { type: NavParams, },
    ];
    return LangSelectModal;
}());
