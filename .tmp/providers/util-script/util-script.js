/**
    Author (s): Ankit Maheshwari,
                Pradeep Choudhary
    Date: 22Oct2016
    Copyright Notice:
    @(#)
    Description:
*/
import { Injectable } from '@angular/core';
import { NavController } from 'ionic-angular';
export var UtilScript = (function () {
    function UtilScript(nav) {
        this.nav = nav;
        this.nav = nav;
    }
    UtilScript.prototype.goBack = function () {
        // navigates to the previous page in the stack,
        // which based on the model above is tab1Inner
        this.nav.pop();
    };
    UtilScript.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    UtilScript.ctorParameters = [
        { type: NavController, },
    ];
    return UtilScript;
}());
